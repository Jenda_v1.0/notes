# Notes

School project for UMTE subject.


## Info

Use the application to write notes.

You need to create a board to create topics that represent categories. You can create a note for each topic that can be marked as completed. Alternatively, the topic itself can be considered as a comment (if necessary). For each created object (board, topic, note) it is obligatory to enter a title and optional description. Furthermore, the creation date and the date of the last edit are automatically written. A user can create their own notes or create a team to add additional users to. The team can create boards that are accessible to all team members with both read and write rights.


### Procedure

To use the application, you need to have an account with a verified email address. After the account is created, a verification email is sent or it can be sent repeatedly in the profile, but if the email is not verified, the user will not see any of his / her data after signing in.

When the application is launched, a screen will be displayed where the user will be able to sign in or go to registration activity. In the case of registration, it is necessary to fill in all mandatory fields and after confirmation it is necessary to verify the email. The verification email will be sent in about 5 minutes after creating the account.

When you sign in, you'll see a welcome screen with all your private boards. At the bottom right of the activity is a 'plus' icon representing the ability to create a new notice board. You can then go directly to the board by clicking directly on the wanted board and then on the topics and from the topics on the notes. In all cases (board, topic and note) there is an option to create, edit or delete it.

On the left side of the screen, you can see a pop-up menu that lets you switch to boards, teams, profile, or sign out. The options for your account are available in your profile. Such as editing email, username, user name and surname, phone number - optional. Next, change password. Clicking on this button will send a password change link to the specified email. E-mail address verification option. When you click this button, a verification email will be sent to the address you entered and must be confirmed.

At the end of the cancellation / deletion of the account. Clicking on this button will delete all data related to the account. For example, the above profile information, any boards he has created and will be removed as a member of all teams If any team member leaves, app will find out if there is at least one member in the team. If not, the team will be removed with all of its boards.

Every board created has its author, so even if the board was created within the team. If the user who created the board is deleted, it will also be deleted from the team. *(Here it would be possible to test if the board is not in the team, but there is an unnecessarily lengthy implementation (Firebase), so leave this to be 'school project only.')

If you delete a board, its topics will be deleted and their notes will be deleted for each topic. In case of deleting topic its notes will be deleted and in case of deletion of the note only the note itself will be deleted.


### Backend

The backend part is Firebase, which is used for authentication and data management (NoSql database). The database can be empty and there is no need to configure Firebase. Entities in the `cz.uhk.umte.kruncja1.notes.database.entity` package are stored in the database. To make the database operational, it was necessary to replace false with true in the Firebase console -> Database -> Rules.

In the database, the data is linked using document / record id. To connect to the user, the generated `User UID` was used, by means of which, for example, boards can be assigned to a user, similarly to teams etc.