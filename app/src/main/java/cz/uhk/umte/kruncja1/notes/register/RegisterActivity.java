package cz.uhk.umte.kruncja1.notes.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

import cz.uhk.umte.kruncja1.notes.MainActivity;
import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.register.validation.FormDataValidation;
import cz.uhk.umte.kruncja1.notes.register.validation.FormDataValidationImpl;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Create (register) a new user.
 */

public class RegisterActivity extends AppCompatActivity {

    private EditText txtEmail, txtUsername, txtFirstName, txtLastName, txtTelephoneNumber, txtPassword1, txtPassword2;

    private TextValidation textValidation;

    private FormDataValidation formDataValidation;

    private FirebaseAuth firebaseAuth;

    private UserDatabase userDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_title_registration));
        setContentView(R.layout.activity_register);

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        // Database operations with users:
        userDatabase = new UserDatabaseImpl();

        textValidation = new TextValidationImpl();
        formDataValidation = new FormDataValidationImpl();

        txtEmail = findViewById(R.id.registerEmailInput);
        txtUsername = findViewById(R.id.usernameInput);
        txtFirstName = findViewById(R.id.firstNameInput);
        txtLastName = findViewById(R.id.lastNameInput);
        txtTelephoneNumber = findViewById(R.id.telephoneNumberInput);
        txtPassword1 = findViewById(R.id.password_1_input);
        txtPassword2 = findViewById(R.id.password_2_input);
    }

    public void onBtnRegisterClick(View view) {
        // Error texts for empty fields:
        final String emptyEmail = getString(R.string.act_register_empty_email);
        final String emptyUsername = getString(R.string.act_register_empty_username);
        final String emptyFirstName = getString(R.string.act_register_empty_first_name);
        final String emptyLastName = getString(R.string.act_register_empty_last_name);
        final String emptyPassword1 = getString(R.string.act_register_empty_password_1);
        final String emptyPassword2 = getString(R.string.act_register_empty_password_2);

        // Error texts for invalidate values:
        final String invalidMinLengthEmail = getString(R.string.act_register_invalid_min_length_email);
        final String invalidMinLengthUsername = getString(R.string.act_register_invalid_min_length_username);
        final String invalidMinLengthFirstName = getString(R.string.act_register_invalid_min_length_first_name);
        final String invalidMinLengthLastName = getString(R.string.act_register_invalid_min_length_last_name);
        final String invalidMinLengthPassword1 = getString(R.string.act_register_invalid_min_length_password_1);
        final String invalidMinLengthPassword2 = getString(R.string.act_register_invalid_min_length_password_2);

        final String invalidMaxLengthEmail = getString(R.string.act_register_invalid_max_length_email);
        final String invalidMaxLengthUsername = getString(R.string.act_register_invalid_max_length_username);
        final String invalidMaxLengthFirstName = getString(R.string.act_register_invalid_max_length_first_name);
        final String invalidMaxLengthLastName = getString(R.string.act_register_invalid_max_length_last_name);
        final String invalidMaxLengthPassword1 = getString(R.string.act_register_invalid_max_length_password_1);
        final String invalidMaxLengthPassword2 = getString(R.string.act_register_invalid_max_length_password_2);

        // Validation results (telephone number is not necessary):
        final boolean validatedEmail = textValidation.validateBasicTextField(txtEmail, emptyEmail, invalidMinLengthEmail, invalidMaxLengthEmail);
        final boolean validatedUsername = textValidation.validateBasicTextField(txtUsername, emptyUsername, invalidMinLengthUsername, invalidMaxLengthUsername);
        final boolean validatedFirstName = textValidation.validateBasicTextField(txtFirstName, emptyFirstName, invalidMinLengthFirstName, invalidMaxLengthFirstName);
        final boolean validatedLastName = textValidation.validateBasicTextField(txtLastName, emptyLastName, invalidMinLengthLastName, invalidMaxLengthLastName);
        final boolean validatedPassword1 = textValidation.validateBasicTextField(txtPassword1, emptyPassword1, invalidMinLengthPassword1, invalidMaxLengthPassword1);
        final boolean validatedPassword2 = textValidation.validateBasicTextField(txtPassword2, emptyPassword2, invalidMinLengthPassword2, invalidMaxLengthPassword2);

        if (!validatedEmail || !validatedUsername || !validatedFirstName || !validatedLastName || !validatedPassword1 || !validatedPassword2) {
            Log.w("REGISTER DATA LENGTH", "Invalid texts length in text fields.");
            return;
        }

        Log.w("REGISTER", "Data in text fields are valid, trying to register new user.");

        // Texts form text fields:
        final String email = textValidation.getTextFromEditText(txtEmail);
        final String username = textValidation.getTextFromEditText(txtUsername);
        final String firstName = textValidation.getTextFromEditText(txtFirstName);
        final String lastName = textValidation.getTextFromEditText(txtLastName);
        final String telephoneNumber = textValidation.getTextFromEditText(txtTelephoneNumber);
        final String password1 = textValidation.getTextFromEditText(txtPassword1);
        final String password2 = textValidation.getTextFromEditText(txtPassword2);

        final String invalidEmail = getString(R.string.act_register_invalid_regex_email);
        final String invalidUsername = getString(R.string.act_register_invalid_regex_username);
        final String invalidFirstName = getString(R.string.act_register_invalid_regex_first_name);
        final String invalidLastName = getString(R.string.act_register_invalid_regex_last_name);
        final String invalidTelephoneNumber = getString(R.string.act_register_invalid_regex_telephone_number);
        final String invalidPasswords = getString(R.string.act_register_invalid_regex_passwords);

        final boolean validatedRegExEmail = formDataValidation.validateEmail(txtEmail, invalidEmail);
        final boolean validatedRegExUsername = formDataValidation.validateUsername(txtUsername, invalidUsername);
        final boolean validatedRegExFirstName = formDataValidation.validateFirstName(txtFirstName, invalidFirstName);
        final boolean validatedRegExLastName = formDataValidation.validateLastName(txtLastName, invalidLastName);
        final boolean validatedRegExTelephoneNumber = formDataValidation.validateTelephoneNumber(txtTelephoneNumber, invalidTelephoneNumber);
        final boolean validatedRegExPasswords = formDataValidation.validatePassword(password1, password2, invalidPasswords, this);

        if (!validatedRegExEmail || !validatedRegExUsername || !validatedRegExFirstName || !validatedRegExLastName || !validatedRegExTelephoneNumber || !validatedRegExPasswords) {
            Log.w("REGISTER TEXTS REG EX", "Invalid data in text fields.");
            return;
        }


        // Attempting to create a new user.
        firebaseAuth.createUserWithEmailAndPassword(email, password1).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful() && firebaseAuth.getCurrentUser() != null) {
                    // If the registration is successful, the activity with user board overview will start:
                    Log.w("REGISTRATION", "New user account successfully created, starting MainActivity.");

                    // Sending verification email:
                    firebaseAuth.getCurrentUser().sendEmailVerification()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.w("SEND VERIFICATION EMAIL", "Verification email was successfully sent.");
                                    ToastHelper.showToast(RegisterActivity.this, getString(R.string.act_register_verification_email_send_success));

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w("SEND VERIFICATION EMAIL", "There was an error trying to send a verification email.");
                                    ToastHelper.showToast(RegisterActivity.this, getString(R.string.act_register_verification_email_send_failure));
                                }
                            });

                    final String uid = firebaseAuth.getCurrentUser().getUid();

                    // Phone number in correct data type or default value:
                    final long lTelephoneNumber = userDatabase.parseTelephoneNumber(telephoneNumber);

                    // Create user record in user table:
                    userDatabase.add(new User(uid, username, firstName, lastName, email, lTelephoneNumber), RegisterActivity.this);

                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    finish();
                } else {
                    Log.w("REGISTRATION", "An error occurred while trying to create new account with email and password.");

                    try {
                        throw task.getException();
                    }
                    // If user enters wrong email:
                    catch (FirebaseAuthWeakPasswordException weakPassword) {
                        Log.w("REGISTRATION", "Weak password.");
                        final String txtWeakPassword = getString(R.string.act_register_toast_weak_password);
                        showToast(txtWeakPassword);
                    }
                    // if user enters wrong password.
                    catch (FirebaseAuthInvalidCredentialsException malformedEmail) {
                        Log.w("REGISTRATION", "Malformed email.");
                        final String txtMalformedEmail = getString(R.string.act_register_toast_malformed_email);
                        showToast(txtMalformedEmail);
                    } catch (FirebaseAuthUserCollisionException existEmail) {
                        Log.w("REGISTRATION", "Email collision (email already exist).");
                        final String txtExistEmail = getString(R.string.act_register_toast_email_exist);
                        showToast(txtExistEmail);
                    } catch (Exception e) {
                        Log.w("REGISTRATION", e.getMessage());
                        final String txtInvalidData = getString(R.string.act_register_toast_invalid_data);
                        showToast(txtInvalidData);
                    }
                }
            }
        });
    }

    private void showToast(String text) {
        ToastHelper.showToast(RegisterActivity.this, text);
    }
}