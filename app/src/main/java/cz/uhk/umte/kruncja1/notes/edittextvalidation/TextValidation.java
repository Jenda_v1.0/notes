package cz.uhk.umte.kruncja1.notes.edittextvalidation;

import android.widget.EditText;

/**
 * Validation of values in text fields and auxiliary methods for working with text.
 */

public interface TextValidation {

    /**
     * Check if the text field is empty (contains no value).
     *
     * <i>if the text field is empty, the icon will appear to indicate that the text field is
     * empty.</i>
     *
     * @param editText
     *         text field that it´s value is to be validated (see if it is empty).
     * @param errorText
     *         error message that tells the user that the text field needs to be filled.
     *
     * @return true if the text field is not empty (contains "some" value), otherwise false.
     */
    boolean validateEmptyField(EditText editText, String errorText);

    /**
     * Check if the text field is not empty and the text contains at least the minimum count of
     * characters (cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl#MINIMAL_LENGTH)
     * and text contains max (cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl#MAXIMAL_LENGTH)
     * characters.
     *
     * @param editText
     *         text field from which the text for the above-mentioned validation is taken.
     * @param errorEmptyText
     *         empty field error message
     * @param errorMinLengthText
     *         error message for text less than the allowed minimum
     * @param errorMaxLengthText
     *         error message for text greater than the allowed maximum
     *
     * @return true if text field is not empty and text is greater than or equal to required
     * minimum.
     */
    boolean validateBasicTextField(EditText editText, String errorEmptyText, String errorMinLengthText, String errorMaxLengthText);

    /**
     * Gets text from the text field.
     *
     * @param editText
     *         text field from which the text will be returned.
     *
     * @return text form the text field (editText).
     */
    String getTextFromEditText(EditText editText);
}