package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Note;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Database operations with notes.
 *
 * <i>The Firebase database can be empty; after the first entity is inserted, the necessary
 * structure is created.</i>
 */

public class NoteDatabaseImpl implements NoteDatabase {

    @Override
    public void add(Note note, final Context context, OnSuccessListener<DocumentReference> successListener) {
        Log.w("ADD NOTE", "Add new note to DB: " + note);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.NOTE.getName())
                .add(note)
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ADD NOTE FAILURE", "Failed to create new note.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_add_note_failure));
                    }
                });
    }

    @Override
    public void update(String documentId, final Note note, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("UPDATE NOTE", "Update existing note in DB with id: '" + documentId + "', new note: " + note);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        final DocumentReference document = db.collection(DbCollection.NOTE.getName()).document(documentId);

        document.update("topicUid", note.getTopicUid());// not necessary
        document.update("name", note.getName());
        document.update("description", note.getDescription());
        // (createdAt does not change)
        document.update("lastEditAt", Timestamp.now())
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("UPDATE NOTE FAILURE", "Failed to update existing note: " + note);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_update_note_failure));
                    }
                });
    }

    @Override
    public void updateDoneAttribute(final String documentId, boolean done, final Context context) {
        Log.w("UPDATE NOTE DONE ATTR.", "Update attribute 'done' in existing note in DB with id: '" + documentId + "'.");

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        final DocumentReference document = db.collection(DbCollection.NOTE.getName()).document(documentId);

        document.update("done", done);
        document.update("lastEditAt", Timestamp.now())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w("UPDATE NOTE DONE ATTR.", "'done' attribute in existing note with uid '" + documentId + "' was successfully updated.");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("UPDATE NOTE DONE ATTR.", "Failed to update 'done' attribute in existing note with uid: " + documentId);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_update_note_done_attribute_failure));
                    }
                });
    }

    @Override
    public void getByTopicUid(String topicUid, OnCompleteListener<QuerySnapshot> completeListener) {
        Log.w("GET NOTES BY TOPIC UID", "Get notes related to topic uid: " + topicUid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.NOTE.getName())
                .whereEqualTo("topicUid", topicUid)
                .get()
                .addOnCompleteListener(completeListener);
    }

    @Override
    public void deleteByDocumentId(final String noteUid, final Context context) {
        Log.w("DELETE NOTE BY DOC. ID", "Delete note with its document id: " + noteUid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.NOTE.getName())
                .document(noteUid)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("DELETE NOTE BY DOC. ID", "Note successfully deleted.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_note_success));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DELETE NOTE BY DOC. ID", "An error occurred while trying to delete note with id: " + noteUid, e);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_note_failure));
                    }
                });
    }
}