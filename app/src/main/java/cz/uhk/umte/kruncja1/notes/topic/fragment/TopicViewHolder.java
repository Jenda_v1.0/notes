package cz.uhk.umte.kruncja1.notes.topic.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;

/**
 * This is used to set values ​​to one specific item representing the topic in the displayed list.
 *
 * <i>Through the appropriate methods, the particular topic data that is displayed is set to the
 * component to be rendered.</i>
 */

class TopicViewHolder extends RecyclerView.ViewHolder {

    private TextView tvTopicName, tvTopicDescription;

    private Button btnEditTopic, btnDeleteTopic;

    TopicViewHolder(@NonNull View itemView) {
        super(itemView);

        tvTopicName = itemView.findViewById(R.id.itemTopicName);
        tvTopicDescription = itemView.findViewById(R.id.itemTopicDescription);

        btnEditTopic = itemView.findViewById(R.id.btnEditTopic);
        btnDeleteTopic = itemView.findViewById(R.id.btnDeleteTopic);
    }

    void setTopicData(final String topicDocumentId, final Topic topic, View.OnClickListener deleteTopicOnClickListener, final OnItemTopicClick onItemTopicClick) {
        tvTopicName.setText(topic.getName());
        tvTopicDescription.setText(topic.getDescription());

        btnDeleteTopic.setOnClickListener(deleteTopicOnClickListener);

        btnEditTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTopicClick.onEditTopicClick(topicDocumentId, topic);
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTopicClick.onTopicClick(topicDocumentId, topic);
            }
        });
    }
}