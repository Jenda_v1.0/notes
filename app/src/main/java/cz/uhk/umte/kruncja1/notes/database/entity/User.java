package cz.uhk.umte.kruncja1.notes.database.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

import java.util.Map;

import lombok.Data;

/**
 * DB Entity.
 *
 * <i>Email and password are listed for an account created by creating an email and password
 * account.</i>
 *
 * <i>There is no need to have any data in the firebase database, and after inserting this entity,
 * the necessary data and entities in the database will be automatically created.</i>
 *
 * <i>Timestamp cannot be serialized, so Parcelable is used: https://medium.com/@dbottillo/android-parcel-data-inside-and-between-activities-f1830376b1d0
 * and https://developer.android.com/reference/android/os/Parcelable</i>
 */

@Data
public class User implements Parcelable {

    /**
     * User UID (acquired when creating a user [account] with email and password).
     */
    private String userUid;

    private String username;

    private String firstName;

    private String lastName;

    /**
     * It is needed to identify team members. You could use the 'Firebase Admin SDK', but a lengthy
     * and unnecessary implementation and configuration, and it will certainly be necessary to
     * resolve bugs in versions, etc.
     */
    private String email;

    private Long telephoneNumber;

    private Timestamp createdAt;

    private Timestamp lastEditAt;

    public User() {
        // This constructor is needed for FireStore.
        // (Each custom class must have a public constructor that takes no arguments. In addition, the class must include a public getter for each property)

        // The default value to determine whether it is an existing / valid user.
        userUid = "-1";
    }

    public User(String userUid, String username, String firstName, String lastName, String email, Long telephoneNumber) {
        this.userUid = userUid;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.telephoneNumber = telephoneNumber;

        final Timestamp timestamp = Timestamp.now();
        this.createdAt = timestamp;
        this.lastEditAt = timestamp;
    }

    private User(String userUid, String username, String firstName, String lastName, String email, Long telephoneNumber, Timestamp createdAt, Timestamp lastEditAt) {
        this.userUid = userUid;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.createdAt = createdAt;
        this.lastEditAt = lastEditAt;
    }

    /**
     * Creates the {@link User} object from the data (attribute values) that are on the map.
     *
     * @param data
     *         attributes values.
     *
     * @return cz.uhk.umte.kruncja1.notes.database.entity.User instance containing values from the
     * data map.
     */
    public static User getUserFromMap(Map<String, Object> data) {
        final String userUid = (String) data.get("userUid");
        final String username = (String) data.get("username");
        final String firstName = (String) data.get("firstName");
        final String lastName = (String) data.get("lastName");
        final String email = (String) data.get("email");
        final Long telephoneNumber = (Long) data.get("telephoneNumber");
        final Timestamp createdAt = (Timestamp) data.get("createdAt");
        final Timestamp lastEditAt = (Timestamp) data.get("lastEditAt");

        return new User(userUid, username, firstName, lastName, email, telephoneNumber, createdAt, lastEditAt);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // This is needed (done) when passing an instance of this class to Intent, so you need to "write" all values ​​(serialization replacement because TimeStamp is not serializable):
        dest.writeString(userUid);
        dest.writeString(username);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeLong(telephoneNumber);

        dest.writeInt(createdAt.getNanoseconds());
        dest.writeLong(createdAt.getSeconds());

        dest.writeInt(lastEditAt.getNanoseconds());
        dest.writeLong(lastEditAt.getSeconds());
    }

    private User(Parcel in) {
        userUid = in.readString();
        username = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        telephoneNumber = in.readLong();

        final int iCreatedAt = in.readInt();
        final long lCreatedAt = in.readLong();
        createdAt = new Timestamp(lCreatedAt, iCreatedAt);

        final int iLastEditAt = in.readInt();
        final long lLastEditAt = in.readLong();
        lastEditAt = new Timestamp(lLastEditAt, iLastEditAt);
    }
}