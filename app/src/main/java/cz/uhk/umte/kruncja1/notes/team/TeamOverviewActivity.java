package cz.uhk.umte.kruncja1.notes.team;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.activity.ActionKind;
import cz.uhk.umte.kruncja1.notes.board.BoardActivity;
import cz.uhk.umte.kruncja1.notes.board.fragment.BoardFragment;
import cz.uhk.umte.kruncja1.notes.board.fragment.OnItemBoardClick;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

import static cz.uhk.umte.kruncja1.notes.texthelper.TextHelper.getTextFromTimeStamp;

/**
 * The activity is used to display information about the selected team. Furthermore, all boards
 * created within the team (but by a specific user) are displayed.
 */

public class TeamOverviewActivity extends AppCompatActivity implements OnItemBoardClick {

    private Map<String, Board> boards;

    private Team selectedTeam;
    private String teamDocumentId;

    private BoardDatabase boardDatabase;

    private BoardFragment boardFragment;

    /**
     * It is needed to pass the UID of the signed in user to the activity to create the board.
     */
    private FirebaseAuth firebaseAuth;

    private TeamDatabase teamDatabase;

    private User signedInUser;
    private String userDocumentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_title_team));
        setContentView(R.layout.activity_team_overview);

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        boardDatabase = new BoardDatabaseImpl();
        teamDatabase = new TeamDatabaseImpl();

        // Retrieve all boards that belong to the transferred team (selectedTeam)
        boards = new LinkedHashMap<>();

        final Intent intent = getIntent();
        selectedTeam = intent.getExtras().getParcelable("selectedTeam");
        teamDocumentId = intent.getStringExtra("teamDocumentId");

        signedInUser = intent.getExtras().getParcelable("signedInUser");
        userDocumentId = intent.getStringExtra("userDocumentId");

        if (selectedTeam != null) {
            Log.w("LOAD BOARDS IN TEAM", "The selected team has been successfully passed, followed by its boards.");

            // Load boards that belong to selectedTeam from database
            loadBoardsByItsDocumentId(selectedTeam.getBoardsUid());

            // Set the selected team information to text fields
            final TextView tvTeamName = findViewById(R.id.atoTeamName);
            final TextView tvTeamDescription = findViewById(R.id.atoTeamDescription);
            final TextView tvTeamCreatedAt = findViewById(R.id.atoTeamCreatedAt);
            final TextView tvTeamLastEditAt = findViewById(R.id.atoTeamLastEditAt);

            tvTeamName.setText(selectedTeam.getName());
            tvTeamDescription.setText(selectedTeam.getDescription());
            tvTeamCreatedAt.setText(getString(R.string.act_team_overview_txt_view_team_created_at).concat(" ").concat(getTextFromTimeStamp(selectedTeam.getCreatedAt())));
            tvTeamLastEditAt.setText(getString(R.string.act_team_overview_txt_view_team_last_edit_at).concat(" ").concat(getTextFromTimeStamp(selectedTeam.getCreatedAt())));
        } else {
            Log.w("LOAD BOARDS IN TEAM", "Failed to pass the selected team to activity, unable to load its bulletin boards.");
            ToastHelper.showToast(this, getString(R.string.act_team_overview_team_has_not_been_passed_to_activity));
        }
    }

    @Override
    public void onEditBoardClick(String boardDocumentId, Board board) {
        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.EDIT);
        boardIntent.putExtra("board", board);
        boardIntent.putExtra("documentId", boardDocumentId);
        boardIntent.putExtra("startIntentAfterEdit", TeamOverviewActivity.class.getCanonicalName());
        boardIntent.putExtra("selectedTeam", selectedTeam);
        boardIntent.putExtra("teamDocumentId", teamDocumentId);
        boardIntent.putExtra("userDocumentId", userDocumentId);
        boardIntent.putExtra("signedInUser", signedInUser);

        startActivity(boardIntent);
    }

    @Override
    public void onBoardClick(String boardDocumentId, Board board) {
        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.SHOW);
        boardIntent.putExtra("board", board);
        boardIntent.putExtra("documentId", boardDocumentId);

        startActivity(boardIntent);
    }

    /**
     * Load boards by its document id (/ record id) from database in boardsUid parameter and save
     * them to boards maps.
     *
     * @param boardsUid
     *         document / record id (of boards) in database that needs to be load
     */
    private void loadBoardsByItsDocumentId(List<String> boardsUid) {
        Log.w("LOAD BOARDS BY ITS D ID", "Loading boards from database that have its document / record id: '" + boardsUid + "'.");

        // "Create" a fragment in which individual boards will show:
        boardFragment = (BoardFragment) getSupportFragmentManager().findFragmentById(R.id.teamBoardFragment);

        for (String boardUid : boardsUid) {
            Log.w("LOAD BOARDS BY ITS D ID", "Loading board from database with document / record id:'" + boardUid + "'.");
            boardDatabase.getByDocumentUid(boardUid, this, new GetBoardInTeamOnSuccessListener());
        }

        if (boardFragment != null) {
            Log.w("LOAD BOARDS BY ITS D ID", "The fragment is successfully created, updating the boards displayed.");
            boardFragment.setData(boards, TeamOverviewActivity.this);
        }
    }

    /**
     * Start activity to create a new board to the selected team.
     *
     * @param view
     */
    public void atoBtnCreateBoardClick(View view) {
        Log.w("CREATE BOARD TO TEAM", "Launch BoardActivity to create a new board for the selected team.");

        final Intent boardIntent = new Intent(this, BoardActivity.class);

        if (firebaseAuth.getCurrentUser() != null) {// not necessary condition
            boardIntent.putExtra("userUid", firebaseAuth.getCurrentUser().getUid());
        }

        boardIntent.putExtra("actionKind", ActionKind.CREATE);
        boardIntent.putExtra("selectedTeam", selectedTeam);
        boardIntent.putExtra("teamDocumentId", teamDocumentId);
        boardIntent.putExtra("userDocumentId", userDocumentId);
        boardIntent.putExtra("signedInUser", signedInUser);

        startActivity(boardIntent);
    }

    public void atoBtnLeaveTeamClick(View view) {
        Log.w("REMOVE USER FROM TEAM", "Remove a user with document id '" + userDocumentId + "' from the team " + selectedTeam);

        teamDatabase.leave(teamDocumentId, userDocumentId, selectedTeam, this, createLeaveTeamSuccessListener());
    }

    private OnSuccessListener<Void> createLeaveTeamSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE TEAM SUCCESS", "Team successfully updated in DB.");
                Log.w("REMOVE USER FROM TEAM", "User with document id '" + userDocumentId + "' was successfully removed from the team.");

                ToastHelper.showToast(TeamOverviewActivity.this, getString(R.string.act_team_overview_update_team_member_successfully_abandoned));

                final Intent teamIntent = new Intent(TeamOverviewActivity.this, TeamActivity.class);

                teamIntent.putExtra("actionKind", ActionKind.SHOW);
                teamIntent.putExtra("userDocumentId", userDocumentId);
                teamIntent.putExtra("signedInUser", signedInUser);

                startActivity(teamIntent);
                finish();
            }
        };
    }

    public void atoBtnEditTeamClick(View view) {
        Log.w("EDIT SELECTED TEAM", "Start TeamActivity for edit selected team.");

        final Intent teamIntent = new Intent(this, TeamActivity.class);

        teamIntent.putExtra("actionKind", ActionKind.EDIT);
        teamIntent.putExtra("signedInUser", signedInUser);
        teamIntent.putExtra("userDocumentId", userDocumentId);
        teamIntent.putExtra("team", selectedTeam);
        teamIntent.putExtra("teamDocumentId", teamDocumentId);

        startActivity(teamIntent);
    }

    public void atoBtnDeleteTeamClick(View view) {
        Log.w("DELETE SELECTED TEAM", "Delete selected team.");

        teamDatabase.deleteByDocumentId(teamDocumentId, selectedTeam.getBoardsUid(), this);
    }

    /**
     * Listener that will successfully loaded board (the object with appropriate document / record
     * id) from the database save to the map and update the currently displayed boards in the
     * selected team.
     */
    private class GetBoardInTeamOnSuccessListener implements OnSuccessListener<DocumentSnapshot> {

        @Override
        public void onSuccess(DocumentSnapshot documentSnapshot) {
            Log.w("SAVE LOADED BOARD", "Board with document /record id '" + documentSnapshot.getId() + "' has been successfully loaded. It is then inserted into the map.");

            final Board board = documentSnapshot.toObject(Board.class);
            if (board != null) {
                Log.w("SAVE LOADED BOARD", "The appropriate board object has been successfully created. The board will be inserted into the map and then displayed to the user.");

                boards.put(documentSnapshot.getId(), board);
                // Výše se do mapy vložila nově načtená nástěnky, je třeba aktualizovat fragment:
                boardFragment.updateBoardFragment();
            }
        }
    }
}