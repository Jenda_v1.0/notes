package cz.uhk.umte.kruncja1.notes.note.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Note;

/**
 * This class is in charge (the quick rewriting of the data currently being displayed to the user),
 * which is set in the activity where the relevant data is to be displayed (in this case, notes).
 */

public class NoteFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Loading content to view from xml file:
        return inflater.inflate(R.layout.fragment_note_list, container, false);
    }

    /**
     * Set the necessary data and operations for the components that are currently displayed.
     *
     * @param notes
     *         all notes to display (the current notes will be redrawn in a certain order as user
     *         scroll)
     * @param onItemNoteClick
     *         some events on buttons
     */
    public void setData(Map<String, Note> notes, OnItemNoteClick onItemNoteClick) {
        // The view will be created, onCreateView will be created before this setData method is called, so the necessary values ​​are created here:
        final RecyclerView recyclerView = getView().findViewById(R.id.noteRecyclerView);

        final RecyclerView.Adapter adapter = new NoteAdapter(notes, onItemNoteClick);
        recyclerView.setAdapter(adapter);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }
}