package cz.uhk.umte.kruncja1.notes.database.entity;

import android.os.Parcelable;

import com.google.firebase.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Common variables and methods for database entities.
 *
 * <i>Timestamp cannot be serialized, so Parcelable is used: https://medium.com/@dbottillo/android-parcel-data-inside-and-between-activities-f1830376b1d0
 * and https://developer.android.com/reference/android/os/Parcelable</i>
 */

@Data
@NoArgsConstructor
abstract class AbstractEntity implements Parcelable {

    protected String name;

    protected String description;

    Timestamp createdAt;

    Timestamp lastEditAt;

    AbstractEntity(String name, String description) {
        this.name = name;
        this.description = description;

        final Timestamp timestamp = Timestamp.now();
        createdAt = timestamp;
        lastEditAt = timestamp;
    }

    AbstractEntity(String name, String description, Timestamp createdAt, Timestamp lastEditAt) {
        this.name = name;
        this.description = description;
        this.createdAt = createdAt;
        this.lastEditAt = lastEditAt;
    }
}