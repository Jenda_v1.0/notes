package cz.uhk.umte.kruncja1.notes.team.fragment;

import cz.uhk.umte.kruncja1.notes.database.entity.Team;

/**
 * Methods to display data about one selected team or to edit a selected team.
 */

public interface OnItemTeamClick {

    /**
     * Signed in user leaves team.
     *
     * @param teamDocumentId
     *         id document (team) in database
     * @param team
     *         team that the user wants to leave
     */
    void onLeaveTeamClick(final String teamDocumentId, final Team team);

    /**
     * The activity for editing the selected team opens.
     *
     * @param teamDocumentId
     *         id document (team) in database
     * @param team
     *         team that the user wants to edit (passes to activity)
     */
    void onEditTeamClick(final String teamDocumentId, final Team team);

    /**
     * The activity to display information about the selected team opens.
     *
     * @param teamDocumentId
     *         id document (team) in database
     * @param team
     *         team to be passed to the activity to display its data for user
     */
    void onTeamClick(final String teamDocumentId, final Team team);
}