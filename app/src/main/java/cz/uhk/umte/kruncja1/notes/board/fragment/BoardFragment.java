package cz.uhk.umte.kruncja1.notes.board.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;

/**
 * This class is in charge (the quick rewriting of the data currently being displayed to the user),
 * which is set in the activity where the relevant data is to be displayed (in this case, boards).
 */

public class BoardFragment extends Fragment {

    private BoardAdapter boardAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Loading content to view from xml file:
        return inflater.inflate(R.layout.fragment_board_list, container, false);
    }

    /**
     * Set the necessary data and operations for the components that are currently displayed.
     *
     * @param boards
     *         all boards to display (the current boards will be redrawn in a certain order as user
     *         scroll)
     * @param onItemBoardClick
     *         some events on buttons
     */
    public void setData(Map<String, Board> boards, OnItemBoardClick onItemBoardClick) {
        // The view will be created, onCreateView will be created before this setData method is called, so the necessary values ​​are created here:
        final RecyclerView recyclerView = getView().findViewById(R.id.boardRecyclerView);

        boardAdapter = new BoardAdapter(boards, onItemBoardClick);
        recyclerView.setAdapter(boardAdapter);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    /**
     * Updates items (boards) displayed in the fragment.
     */
    public void updateBoardFragment() {
        if (boardAdapter != null) {
            boardAdapter.updateItems();
        }
    }
}