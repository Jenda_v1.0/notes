package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Implementation of database operations with users.
 * <p>
 * Resources: https://firebase.google.com/docs/firestore/query-data/get-data
 * <p>
 * CRUD: https://firebase.google.com/docs/firestore/manage-data/add-data?authuser=0
 */

public class UserDatabaseImpl implements UserDatabase {

    private static final BoardDatabase BOARD_DATABASE = new BoardDatabaseImpl();

    @Override
    public void add(User user, final Context context) {
        Log.w("ADD USER", "Add new user to DB: " + user);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // If the document id was entered as a user uid, this way, a record with id as the user's uid can be created (add success and failure listeners).
        // (According to this id (uid), a record of one particular user would then be searched for.)
//        db.collection(DbCollection.USER.getName())
//                // Setting a unique document ID to access the particular record:
//                .document(user.getUserUid())
//                .set(user);

        db.collection(DbCollection.USER.getName())
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.w("ADD USER SUCCESS", "User successfully created / added to DB.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_add_user_success));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ADD USER FAILURE", "Failed to add new user to DB.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_add_user_failure));
                    }
                });
    }

    @Override
    public void get(String uid, OnCompleteListener<QuerySnapshot> completeListener) {
        Log.w("GET USER", "Get user with UID: " + uid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // If the document id was entered as a user uid, this way you can always find one record (by uid), necessary edit listener:
//        final DocumentReference docRef = db.collection(DbCollection.USER.getName()).document(uid);
//        docRef.get().addOnCompleteListener(completeListener);

        db.collection(DbCollection.USER.getName())
                .whereEqualTo("userUid", uid)
                .get()
                .addOnCompleteListener(completeListener);
    }

    @Override
    public void delete(final String uid, String userUid, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("DELETE USER BY DOC. ID", "Delete user with its document id: " + uid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // Delete all boards that belong to the topic with id to be deleted:
        // (For each board, its data, ie topics and notes, is deleted.)
        db.collection(DbCollection.BOARD.getName())
                .whereEqualTo("ownerUid", userUid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.w("GET BOARDS BY USER UID", "All boards with ownerUid '" + uid + "' successfully found.");

                            for (final QueryDocumentSnapshot document : task.getResult()) {
                                Log.w("DELETE BOARD", "Delete board with uid: " + document.getId());
                                // Delete board by its document id:
                                BOARD_DATABASE.deleteByDocumentId(document.getId(), context);
                            }
                        } else {
                            Log.w("GET BOARDS BY USER UID", "Failed to get boards with ownerUid: " + uid);
                            ToastHelper.showToast(context, context.getString(R.string.db_impl_obtain_boards_in_delete_boards_failed));
                        }
                    }
                });


        // Delete user with uid as document ID in database:
        db.collection(DbCollection.USER.getName())
                .document(uid)
                .delete()
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DELETE USER BY DOC. ID", "An error occurred while trying to delete user with documnt uid: " + uid, e);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_user_failure));
                    }
                });
    }

    @Override
    public void update(String userDocumentId, final User user, final Context context) {
        final OnSuccessListener<Void> onSuccessListener = new OnSuccessListener<Void>() {

            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE USER SUCCESS", "User successfully updated in DB.");
            }
        };

        updateUser(userDocumentId, user, context, onSuccessListener);
    }

    @Override
    public void update(String userDocumentId, User user, Context context, OnSuccessListener<Void> successListener) {
        updateUser(userDocumentId, user, context, successListener);
    }

    @Override
    public Long parseTelephoneNumber(String telephoneNumber) {
        final String number = telephoneNumber.replaceAll("\\s*", "");

        if (!number.equals("")) {
            return Long.parseLong(number);
        }
        return -1L;
    }

    private static void updateUser(String userDocumentId, final User user, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("UPDATE USER", "Update existing user in DB with id: '" + userDocumentId + "', new user: " + user);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        final DocumentReference document = db.collection(DbCollection.USER.getName()).document(userDocumentId);

        // For a change to take effect when a user is viewed in a profile:
        user.setLastEditAt(Timestamp.now());

        document.update("userUid", user.getUserUid());// not necessary
        document.update("username", user.getUsername());
        document.update("firstName", user.getFirstName());
        document.update("lastName", user.getLastName());
        document.update("email", user.getEmail());
        document.update("telephoneNumber", user.getTelephoneNumber());
        // (createdAt does not change)
        document.update("lastEditAt", user.getLastEditAt())
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("UPDATE USER FAILURE", "Failed to update existing user: " + user);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_update_existing_user_failure));
                    }
                });
    }
}