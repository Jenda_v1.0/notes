package cz.uhk.umte.kruncja1.notes.team.member;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import cz.uhk.umte.kruncja1.notes.R;

/**
 * Set up one displayed user email in the team creation and editing form.
 * <p>
 * https://stackoverflow.com/questions/20750118/displaying-list-of-strings-in-android
 */

public class MemberArrayAdapter extends ArrayAdapter<String> {

    private final List<String> memberEmails;

    /**
     * The currently signed in user's email. This email cannot be deleted because it would not be
     * able to edit the team, so he will only be able to leave it.
     */
    private String signedInUserEmail;

    public MemberArrayAdapter(Context context, List<String> memberEmails, String signedInUserEmail) {
        super(context, R.layout.member_array_row_layout, memberEmails);
        this.memberEmails = memberEmails;
        this.signedInUserEmail = signedInUserEmail;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final View rowView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.member_array_row_layout, parent, false);

        final String memberEmail = memberEmails.get(position);

        // Displayed user (his email)
        final TextView textView = rowView.findViewById(R.id.lblUserEmail);
        textView.setText(memberEmail);

        final Button btnDeleteMember = rowView.findViewById(R.id.marlBtnDeleteMember);
        // The signed in user will not be able to remove himself from the team:
        if (memberEmail.equals(signedInUserEmail)) {
            btnDeleteMember.setEnabled(false);
        }

        btnDeleteMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memberEmails.remove(position);
                // To reflect the changes in the view:
                notifyDataSetChanged();
            }
        });

        return rowView;
    }

    public void addMemberEmail(String memberEmail) {
        memberEmails.add(memberEmail);
        notifyDataSetChanged();
    }
}