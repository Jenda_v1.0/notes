package cz.uhk.umte.kruncja1.notes.activity;

import cz.uhk.umte.kruncja1.notes.board.BoardActivity;

/**
 * Whether {@link BoardActivity}, {@link cz.uhk.umte.kruncja1.notes.topic.TopicActivity} or {@link
 * cz.uhk.umte.kruncja1.notes.note.NoteActivity} activities should display components to create a
 * new or edit an existing object, or just display the data for that object.
 */

public enum ActionKind {

    /**
     * View components to enter the data needed to create a new board.
     */
    CREATE,
    /**
     * View components to enter the data needed to edit an existing board.
     */
    EDIT,
    /**
     * The components that display information about the selected board will be displayed.
     */
    SHOW
}