package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.database.entity.Topic;

/**
 * Database operations with topics.
 *
 * <i>The Firebase database can be empty; after the first entity is inserted, the necessary
 * structure is created.</i>
 */

public interface TopicDatabase {

    /**
     * Create a new topic in the database (create a new record in the database).
     *
     * @param topic
     *         new topic to create (insert as a new record in the database).
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully create a DB record, which then displays all topics belonging
     *         to the selected board of signed in user.
     */
    void add(Topic topic, final Context context, OnSuccessListener<DocumentReference> successListener);

    /**
     * Updating an existing topic in the database.
     *
     * @param documentId
     *         document id / existing topic in the database to be updated.
     * @param topic
     *         topic with new data.
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully update a DB record, which then displays all topics belonging
     *         to the selected board of signed in user.
     */
    void update(String documentId, final Topic topic, final Context context, OnSuccessListener<Void> successListener);

    /**
     * Obtain all topics belonging to the selected board (board with uid = document id in database)
     * of signed in user.
     *
     * @param uid
     *         the ID of the document in the database of the selected board whose topics are to be
     *         loaded.
     * @param completeListener
     *         listener, which is used to store the topics into the application memory and to report
     *         info about success / failure.
     */
    void getByBoardUid(String uid, OnCompleteListener<QuerySnapshot> completeListener);

    /**
     * Deleting the topic and all data related to it from the database.
     *
     * @param id
     *         document / record id in the database to be deleted.
     * @param context
     *         is needed to display success / failure reports (Toast).
     */
    void deleteByDocumentId(String id, final Context context);
}