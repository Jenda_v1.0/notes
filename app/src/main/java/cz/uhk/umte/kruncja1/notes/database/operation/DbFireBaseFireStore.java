package cz.uhk.umte.kruncja1.notes.database.operation;

import com.google.firebase.firestore.FirebaseFirestore;

class DbFireBaseFireStore {

    static FirebaseFirestore getDbConnection() {
        // For some reason, it must be initiated here, otherwise it does not work (passing in constructor, method parameter, etc.)
        return FirebaseFirestore.getInstance();
    }
}