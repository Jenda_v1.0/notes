package cz.uhk.umte.kruncja1.notes.team;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.activity.ActionKind;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.register.validation.FormDataValidation;
import cz.uhk.umte.kruncja1.notes.register.validation.FormDataValidationImpl;
import cz.uhk.umte.kruncja1.notes.team.fragment.OnItemTeamClick;
import cz.uhk.umte.kruncja1.notes.team.fragment.TeamFragment;
import cz.uhk.umte.kruncja1.notes.team.member.MemberArrayAdapter;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Create, edit and display teams, the signed in user is a member of.
 */

public class TeamActivity extends AppCompatActivity implements OnItemTeamClick {

    private TextValidation textValidation;

    private EditText etTeamName, etTeamDescription;

    /**
     * All uses in database collection 'users'.
     */
    private Map<String, User> allUsers;

    private TeamDatabase teamDatabase;

    /**
     * Here, emails of users who are on the team will be added to the form.
     */
    private MemberArrayAdapter memberArrayAdapter;
    /**
     * Emails of users who are or may be members of the specific team (just edited, created, etc.).
     */
    private List<String> memberEmailsList;

    private FormDataValidation formDataValidation;

    private Map<String, Team> teams;

    /**
     * The signed in user in the application is required to get an email to represent the team
     * members, etc.
     */
    private User signedInUser;

    /**
     * You need to load teams, but you need to pass them when viewed so that you can pass them to
     * create and load the views again after you create them.
     */
    private String userDocumentId;

    /**
     * The team you want to edit.
     */
    private Team selectedTeam;
    /**
     * The document ID (team) in the database to modify the document.
     */
    private String teamDocumentId;

    private TeamFragment teamFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        teamDatabase = new TeamDatabaseImpl();

        final Intent intent = getIntent();
        final ActionKind actionkind = (ActionKind) intent.getSerializableExtra("actionKind");
        signedInUser = intent.getExtras().getParcelable("signedInUser");
        userDocumentId = intent.getStringExtra("userDocumentId");

        if (actionkind == ActionKind.CREATE) {
            setTitle(getString(R.string.app_title_create_team));
            setContentView(R.layout.activity_create_team);

            etTeamName = findViewById(R.id.teamNameCreateInput);
            etTeamDescription = findViewById(R.id.teamDescriptionCreateInput);

            allUsers = new LinkedHashMap<>();
            // All users from the database are loaded (to be added to the team by email):
            teamDatabase.getAllUsersInCollection(new GetAllUsersInCollectionOnCompleteListener());

            memberEmailsList = new ArrayList<>();
            memberEmailsList.add(signedInUser.getEmail());
            memberArrayAdapter = new MemberArrayAdapter(this, memberEmailsList, signedInUser.getEmail());
            final ListView listView = findViewById(R.id.membersListView);
            listView.setAdapter(memberArrayAdapter);
        } else if (actionkind == ActionKind.EDIT) {
            setContentView(R.layout.activity_edit_team);

            selectedTeam = intent.getExtras().getParcelable("team");
            teamDocumentId = intent.getStringExtra("teamDocumentId");

            etTeamName = findViewById(R.id.teamNameEditInput);
            etTeamDescription = findViewById(R.id.teamDescriptionEditInput);

            etTeamName.setText(selectedTeam.getName());
            etTeamDescription.setText(selectedTeam.getDescription());

            setTitle(getString(R.string.app_title_edit_text) + "'" + selectedTeam.getName() + "'");

            allUsers = new LinkedHashMap<>();
            // All users from the database are loaded (to be added to the team by email):
            teamDatabase.getAllUsersInCollection(new GetAllUsersAndPrepareMemberEmailsInCollectionOnCompleteListener());

            memberEmailsList = new ArrayList<>();
            memberArrayAdapter = new MemberArrayAdapter(this, memberEmailsList, signedInUser.getEmail());
            final ListView listView = findViewById(R.id.membersListView);
            listView.setAdapter(memberArrayAdapter);
        } else if (actionkind == ActionKind.SHOW) {
            setTitle(getString(R.string.app_title_teams));
            setContentView(R.layout.activity_show_team);

            FirebaseApp.initializeApp(this);
            final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

            teams = new LinkedHashMap<>();

            if (firebaseAuth.getCurrentUser() != null && firebaseAuth.getCurrentUser().isEmailVerified()) {
                Log.w("VERIFIED EMAIL", "The user has verified the email, continuing to load his teams.");
                teamDatabase.getBySignedUserDocumentUid(userDocumentId, new GetTeamsWhereUserIsMemberOnCompleteListener());
            } else {
                Log.w("VERIFIED EMAIL", "User has not verified email.");
                ToastHelper.showToast(this, getString(R.string.act_team_user_has_not_verified_email));
            }
        }

        textValidation = new TextValidationImpl();

        formDataValidation = new FormDataValidationImpl();
    }

    /**
     * When activity_create_team.xml clicks to create a team, the team name is validated and a new
     * database entry is created for it.
     *
     * @param view
     */
    public void onBtnCreateTeamClick(View view) {
        final boolean validatedTeamName = validateTeamName();
        if (!validatedTeamName) {
            Log.w("CREATE NEW TEAM", "Name is not valid.");
            return;
        }

        Log.w("CREATE NEW TEAM", "Name and description is valid, continuing to create new team and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(etTeamName);
        final String description = textValidation.getTextFromEditText(etTeamDescription);

        // UID documents (/ users) in databases to be team members:
        final List<String> userUidList = getUserUid(memberEmailsList, allUsers);

        // Create new Team (add new record to DB):
        final Team team = new Team(userUidList, new ArrayList<String>(), name, description);
        teamDatabase.add(team, this, createAddTeamSuccessListener());
    }

    /**
     * when activity_edit_team.xml clicks on team edit team, the team name is validated here and
     * team members are set up, then changes are saved to the database.
     *
     * @param view
     */
    public void onBtnEditTeamClick(View view) {
        final boolean validatedBoardName = validateTeamName();
        if (!validatedBoardName) {
            Log.w("UPDATE TEAM", "Name is not valid.");
            return;
        }

        Log.w("UPDATE TEAM", "Name and description is valid, continuing to update existing team and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(etTeamName);
        final String description = textValidation.getTextFromEditText(etTeamDescription);

        selectedTeam.setName(name);
        selectedTeam.setDescription(description);

        // UID documents (/ users) in databases to be team members:
        final List<String> userUidList = getUserUid(memberEmailsList, allUsers);
        selectedTeam.setUsersUid(userUidList);

        teamDatabase.update(teamDocumentId, selectedTeam, this, createEditTeamSuccessListener());
    }

    /**
     * Obtaining UIDs of database users who are to be members of a team.
     *
     * @param memberEmailsList
     *         user-specified emails of users who should be on the team.
     * @param allUsers
     *         all users in the database.
     *
     * @return list of UID (users) documents in the database to be / will be team members.
     */
    private static List<String> getUserUid(List<String> memberEmailsList, Map<String, User> allUsers) {
        // UID documents (/ users) in databases to be team members:
        final List<String> userUidList = new ArrayList<>();

        // Obtaining UIDs of database users to be team members:
        for (String memberEmail : memberEmailsList) {
            for (Map.Entry<String, User> entry : allUsers.entrySet()) {
                if (entry.getValue().getEmail().equals(memberEmail.trim())) {
                    userUidList.add(entry.getKey());
                }
            }
        }

        return userUidList;
    }

    /**
     * Validation of the team name entered to etTeamName.
     *
     * @return true if the name is valid, otherwise false.
     */
    private boolean validateTeamName() {
        // Error texts for invalidate values:
        final String emptyName = getString(R.string.act_team_empty_name);
        final String invalidMinLengthName = getString(R.string.act_team_invalid_min_length_name);
        final String invalidMaxLengthName = getString(R.string.act_team_invalid_max_length_name);

        final boolean validatedName = textValidation.validateBasicTextField(etTeamName, emptyName, invalidMinLengthName, invalidMaxLengthName);

        if (!validatedName) {
            Log.w("TEAM NAME LENGTH", "Invalid team name length in text field.");
            return false;
        }

        Log.w("TEAM NAME LENGTH", "Valid team name length in text field.");
        return true;
    }

    /**
     * If the team is successfully created, the {@link TeamActivity} activity will start with the
     * all teams overview.
     *
     * @return a listener performing an action if the team is successfully created (runs an activity
     * with all teams overview).
     */
    private OnSuccessListener<DocumentReference> createAddTeamSuccessListener() {
        return new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.w("ADD TEAM SUCCESS", "Team successfully created / added to DB.");

                ToastHelper.showToast(TeamActivity.this, getString(R.string.act_team_add_team_success));

                startTeamIntent();
            }
        };
    }

    /**
     * If the team is successfully updated, the {@link TeamActivity} will start with the all teams
     * overview.
     *
     * @return a listener performing an action if the team is successfully updated (runs an activity
     * with all teams overview).
     */
    private OnSuccessListener<Void> createEditTeamSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE TEAM SUCCESS", "Team successfully updated in DB.");

                ToastHelper.showToast(TeamActivity.this, getString(R.string.act_team_update_team_success));

                startTeamIntent();
            }
        };
    }

    private void startTeamIntent() {
        Log.w("START TEAM ACTIVITY", "Start TeamActivity with all tams overview (SHOW).");

        final Intent teamIntent = new Intent(this, TeamActivity.class);

        teamIntent.putExtra("actionKind", ActionKind.SHOW);
        teamIntent.putExtra("userDocumentId", userDocumentId);
        teamIntent.putExtra("signedInUser", signedInUser);

        startActivity(teamIntent);
        finish();
    }

    /**
     * When a user clicks on a button to add an email (a new member of a team), he / she validates
     * the email you have entered, or adds it as a new member.
     *
     * @param view
     */
    public void onBtnAddMemberEmailClick(View view) {
        final EditText etMemberEmail = findViewById(R.id.memberEmailEditText);

        // Error messages:
        final String emptyEmail = getString(R.string.act_team_empty_email);
        final String invalidMinLengthEmail = getString(R.string.act_team_invalid_min_length_email);

        final String invalidMaxLengthEmail = getString(R.string.act_team_invalid_max_length_email);
        final boolean validatedEmail = textValidation.validateBasicTextField(etMemberEmail, emptyEmail, invalidMinLengthEmail, invalidMaxLengthEmail);

        if (!validatedEmail) {
            Log.w("EMAIL DATA LENGTH", "Invalid email length in text field.");
            return;
        }

        final String invalidEmail = getString(R.string.act_team_invalid_regex_email);
        final boolean validatedRegExEmail = formDataValidation.validateEmail(etMemberEmail, invalidEmail);

        if (!validatedRegExEmail) {
            Log.w("EMAIL TEXT REG EX", "Invalid email in text field.");
            return;
        }

        final String memberEmail = textValidation.getTextFromEditText(etMemberEmail);
        if (memberEmailsList.contains(memberEmail)) {
            Log.w("DUPLICATE EMAIL", "Email is already entered (duplicate member / email): " + memberEmail);
            etMemberEmail.setError(getString(R.string.act_team_duplicate_email));
            return;
        }

        // Add a new email as a new team member:
        memberArrayAdapter.addMemberEmail(memberEmail);
        etMemberEmail.setText("");
    }

    @Override
    public void onLeaveTeamClick(String teamDocumentId, Team team) {
        Log.w("REMOVE USER FROM TEAM", "Remove a user with document id '" + userDocumentId + "' from the team " + team);

        teamDatabase.leave(teamDocumentId, userDocumentId, team, this, createLeaveTeamSuccessListener(teamDocumentId));
    }

    @Override
    public void onEditTeamClick(String teamDocumentId, Team team) {
        Log.w("EDIT SELECTED TEAM", "Start TeamActivity for edit selected team.");

        final Intent teamIntent = new Intent(this, TeamActivity.class);

        teamIntent.putExtra("actionKind", ActionKind.EDIT);
        teamIntent.putExtra("signedInUser", signedInUser);
        teamIntent.putExtra("userDocumentId", userDocumentId);
        teamIntent.putExtra("team", team);
        teamIntent.putExtra("teamDocumentId", teamDocumentId);

        startActivity(teamIntent);
    }

    @Override
    public void onTeamClick(String teamDocumentId, Team team) {
        Log.w("START TEAM OVERVIEW ATC", "Start TeamOverviewActivity to display details about team.");

        final Intent teamIntent = new Intent(this, TeamOverviewActivity.class);

        teamIntent.putExtra("selectedTeam", team);
        teamIntent.putExtra("teamDocumentId", teamDocumentId);
        teamIntent.putExtra("userDocumentId", userDocumentId);
        teamIntent.putExtra("signedInUser", signedInUser);

        startActivity(teamIntent);
    }

    /**
     * It is called after clicking the button in activity_show_team.xml and it is supposed to start
     * a new team creation activity.
     *
     * @param view
     */
    public void astOnBtnCreateTeamClick(View view) {
        Log.w("START CREATE TEAM ATC", "Start TeamActivity for create new Team.");

        final Intent teamIntent = new Intent(this, TeamActivity.class);

        teamIntent.putExtra("actionKind", ActionKind.CREATE);
        teamIntent.putExtra("signedInUser", signedInUser);
        teamIntent.putExtra("userDocumentId", userDocumentId);

        startActivity(teamIntent);
    }

    /**
     * If the team is successfully updated (/ deleted) (removed its member - signed in user), the
     * displayed teams in the fragment will be updated.
     *
     * @return a listener that removes the team that the signed in user has left. Reload teams in
     * the recycler view to see only those teams where the signed in user is a member and display a
     * message (Toast) that the signed in user has successfully left the team.
     */
    private OnSuccessListener<Void> createLeaveTeamSuccessListener(final String teamDocumentId) {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE TEAM SUCCESS", "Team successfully updated in DB.");
                Log.w("REMOVE USER FROM TEAM", "User with document id '" + userDocumentId + "' was successfully removed from the team.");

                // Removing a team from the Map - the signed in user is no longer a member
                teams.remove(teamDocumentId);

                if (teamFragment != null) {
                    teamFragment.updateTeamFragment();
                }

                ToastHelper.showToast(TeamActivity.this, getString(R.string.act_team_update_team_member_successfully_abandoned));
            }
        };
    }

    /**
     * Listener, which is used to load all users in database. Loaded users will be stored in the
     * allUsers collection.
     */
    private class GetAllUsersInCollectionOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET ALL USERS", "User data was successfully obtained: " + document.getData());

                    final User userFromMap = User.getUserFromMap(document.getData());
                    allUsers.put(document.getId(), userFromMap);
                    Log.w("GET ALL USERS", "Obtained user successfully created: " + userFromMap);
                }
            } else {
                Log.w("GET ALL USERS", "Users data could not be obtained.");
                ToastHelper.showToast(TeamActivity.this, getString(R.string.act_team_users_data_not_obtained));
            }
        }
    }

    /**
     * Listener, which is used to load all users and fill the memberEmailsList collection with the
     * emails of the users in the appropriate team (selectedTeam).
     */
    private class GetAllUsersAndPrepareMemberEmailsInCollectionOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET ALL USERS", "User data was successfully obtained: " + document.getData());

                    final User userFromMap = User.getUserFromMap(document.getData());
                    allUsers.put(document.getId(), userFromMap);
                    Log.w("GET ALL USERS", "Obtained user successfully created: " + userFromMap);
                }

                // All team members will save their emails to the 'memberEmailsList' collection to manipulate team members:
                for (String userDocumentUid : selectedTeam.getUsersUid()) {
                    for (Map.Entry<String, User> entry : allUsers.entrySet()) {
                        if (entry.getKey().equals(userDocumentUid)) {
                            memberEmailsList.add(entry.getValue().getEmail());
                            memberArrayAdapter.notifyDataSetChanged();
                        }
                    }
                }
            } else {
                Log.w("GET ALL USERS", "Users data could not be obtained.");
                ToastHelper.showToast(TeamActivity.this, getString(R.string.act_team_users_data_not_obtained));
            }
        }
    }

    /**
     * The listener is used to store all teams in the 'teams' collection where he / she is a
     * member.
     */
    private class GetTeamsWhereUserIsMemberOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET TEAM BY USER UID", "Team data was successfully obtained: " + document.getData());

                    final Team teamFromMap = Team.getTeamFromMap(document.getData());
                    teams.put(document.getId(), teamFromMap);
                    Log.w("GET TEAM BY USER UID", "Obtained team successfully created: " + teamFromMap);
                }

                Log.w("GET TEAM BY USER UID", "All successfully obtained teams: " + teams);
                // "Create" a fragment in which individual topics will show:
                teamFragment = (TeamFragment) getSupportFragmentManager().findFragmentById(R.id.teamFragment);
                if (teamFragment != null) {
                    teamFragment.setDate(teams, TeamActivity.this);
                }
            } else {
                Log.w("GET TEAM BY USER UID", "Teams data could not be obtained.");
                ToastHelper.showToast(TeamActivity.this, getString(R.string.act_team_teams_data_not_obtained));
            }
        }
    }
}