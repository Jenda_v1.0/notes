package cz.uhk.umte.kruncja1.notes.register.validation;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Implementing methods for validating values ​​in text fields to register a new user.
 */

public class FormDataValidationImpl implements FormDataValidation {

    /**
     * Recommended regex for email addresses.
     * <p>
     * Source: https://howtodoinjava.com/regex/java-regex-validate-email-address/
     */
    private static final String REG_EX_EMAIL = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    private static final String REG_EX_USERNAME = "^[.\\p{L}]+$";
    private static final String REG_EX_FIRST_NAME = "^[.\\p{L}]+$";
    private static final String REG_EX_LAST_NAME = REG_EX_FIRST_NAME;
    private static final String REG_EX_TELEPHONE_NUMBER = "^\\s*\\d{9}\\s*$";

    private final TextValidation textValidation;

    public FormDataValidationImpl() {
        this.textValidation = new TextValidationImpl();
    }

    @Override
    public boolean validateEmail(EditText editText, String errorText) {
        Log.w("VALIDATE EMAIL", "Validate email.");
        return validateTextField(editText, errorText, REG_EX_EMAIL);
    }

    @Override
    public boolean validateUsername(EditText editText, String errorText) {
        Log.w("VALIDATE USERNAME", "Validate username.");
        return validateTextField(editText, errorText, REG_EX_USERNAME);
    }

    @Override
    public boolean validateFirstName(EditText editText, String errorText) {
        Log.w("VALIDATE FIRST NAME", "Validate first name.");
        return validateTextField(editText, errorText, REG_EX_FIRST_NAME);
    }

    @Override
    public boolean validateLastName(EditText editText, String errorText) {
        Log.w("VALIDATE LAST NAME", "Validate last name.");
        return validateTextField(editText, errorText, REG_EX_LAST_NAME);
    }

    @Override
    public boolean validateTelephoneNumber(EditText editText, String errorText) {
        Log.w("VALIDATE PHONE NUMBER", "Validate telephone number.");

        final String text = textValidation.getTextFromEditText(editText).replaceAll("\\s", "");

        if (text.isEmpty()) {
            Log.w("VALIDATE PHONE NUMBER", "Telephone number is not specified (is valid).");
            return true;
        }

        if (!text.matches(REG_EX_TELEPHONE_NUMBER)) {
            editText.setError(errorText);
            return false;
        }
        return true;
    }

    @Override
    public boolean validatePassword(String password1, String password2, String errorText, Context context) {
        Log.w("VALIDATE PASSWORDS", "Validate passwords.");

        if (!password1.equals(password2)) {
            ToastHelper.showToast(context, errorText);
            return false;
        }
        return true;
    }

    /**
     * Validating a value in a text field by regular expression regEx.
     *
     * @param editText
     *         A text field containing the value to be validated.
     * @param errorText
     *         An error message that will be displayed to the user if the value ​​in the text field
     *         (editText) is not valid.
     * @param regEx
     *         A regular expression by which the value in the text field will be validated.
     *
     * @return true if the value in the text field (editTextú is valid, otherwise false.
     */
    private boolean validateTextField(EditText editText, String errorText, String regEx) {
        final String text = textValidation.getTextFromEditText(editText);

        if (!text.matches(regEx)) {
            editText.setError(errorText);
            return false;
        }
        return true;
    }
}