package cz.uhk.umte.kruncja1.notes.board.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;

/**
 * This is used to set values ​​to one specific item representing the board in the displayed list.
 *
 * <i>Through the appropriate methods, the particular board data that is displayed is set to the
 * component to be rendered.</i>
 */

class BoardViewHolder extends RecyclerView.ViewHolder {

    private TextView tvBoardName, tvBoardDescription;

    private Button btnEditBoard, btnDeleteBoard;

    BoardViewHolder(@NonNull View itemView) {
        super(itemView);

        tvBoardName = itemView.findViewById(R.id.itemBoardName);
        tvBoardDescription = itemView.findViewById(R.id.itemBoardDescription);

        btnEditBoard = itemView.findViewById(R.id.btnEditBoard);
        btnDeleteBoard = itemView.findViewById(R.id.btnDeleteBoard);
    }

    void setBoardData(final String boardDocumentId, final Board board, View.OnClickListener deleteBoardListener, final OnItemBoardClick onItemBoardClick) {
        tvBoardName.setText(board.getName());
        tvBoardDescription.setText(board.getDescription());

        btnDeleteBoard.setOnClickListener(deleteBoardListener);

        btnEditBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemBoardClick.onEditBoardClick(boardDocumentId, board);
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemBoardClick.onBoardClick(boardDocumentId, board);
            }
        });
    }
}