package cz.uhk.umte.kruncja1.notes.note.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Note;

/**
 * This is used to set values ​​to one specific item representing the note in the displayed list.
 *
 * <i>Through the appropriate methods, the particular note data that is displayed is set to the
 * component to be rendered.</i>
 */

class NoteViewHolder extends RecyclerView.ViewHolder {

    /**
     * Only the note title will be displayed in the note list within the selected topic because its
     * description may be longer and additional information may be provided, etc.
     */
    private TextView tvNoteName;

    private Button btnEditNote, btnDeleteNote;

    private CheckBox chcbIsDone;

    NoteViewHolder(@NonNull View itemView) {
        super(itemView);

        tvNoteName = itemView.findViewById(R.id.itemNoteName);

        chcbIsDone = itemView.findViewById(R.id.chcbNoteIsDone);

        btnEditNote = itemView.findViewById(R.id.btnEditNote);
        btnDeleteNote = itemView.findViewById(R.id.btnDeleteNote);
    }

    void setNoteData(final String noteDocumentId, final Note note, View.OnClickListener deleteBoardListener, final OnItemNoteClick onItemNoteClick) {
        tvNoteName.setText(note.getName());
        chcbIsDone.setChecked(note.isDone());

        chcbIsDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                note.setDone(chcbIsDone.isChecked());
                onItemNoteClick.onChcbDoneClick(noteDocumentId, chcbIsDone.isChecked());
            }
        });

        btnDeleteNote.setOnClickListener(deleteBoardListener);

        btnEditNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemNoteClick.onEditNoteClick(noteDocumentId, note);
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemNoteClick.onNoteClick(noteDocumentId, note);
            }
        });
    }
}