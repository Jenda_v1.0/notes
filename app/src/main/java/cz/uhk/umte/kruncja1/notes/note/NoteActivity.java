package cz.uhk.umte.kruncja1.notes.note;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.activity.ActionKind;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.entity.Note;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;
import cz.uhk.umte.kruncja1.notes.database.operation.NoteDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.NoteDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.texthelper.TextHelper;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;
import cz.uhk.umte.kruncja1.notes.topic.TopicActivity;

/**
 * Displays all notes of the user selected topic. Functions are always available to either create a
 * new note, edit an existing note, or display information about the selected note.
 */

public class NoteActivity extends AppCompatActivity {

    private TextValidation textValidation;

    private NoteDatabase noteDatabase;

    private EditText txtName, txtDescription;

    /**
     * You need to pass a board here, because if you want to run {@link TopicActivity}, you need to
     * give it a board to show the data (topic) and to forward it, etc.
     */
    private Board selectedBoard;
    /**
     * It is needed for handing over to {@link TopicActivity} and then for editing and other
     * operations.
     */
    private String boardDocumentId;
    /**
     * There is need to pass a topic here, because when you want to run {@link TopicActivity}, you
     * need to give it a topic to be displayed (its detail) to which you will see all the notes,
     * etc.
     */
    private Topic selectedTopic;
    /**
     * ID of the selected topic in the database. It is needed to pass to {@link TopicActivity}.
     */
    private String topicDocumentId;

    /**
     * Note to edit or view its data.
     */
    private Note selectedNote;
    /**
     * It is needed to update the database note (document id in the database).
     */
    private String noteDocumentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Displaying activity content based on whether it is a creating new note or editing an existing note or show data of selected note:
        final Intent intent = getIntent();
        final ActionKind actionkind = (ActionKind) intent.getSerializableExtra("actionKind");
        selectedBoard = intent.getExtras().getParcelable("selectedBoard");
        boardDocumentId = intent.getStringExtra("boardDocumentId");
        selectedTopic = intent.getExtras().getParcelable("selectedTopic");
        topicDocumentId = intent.getStringExtra("topicDocumentId");

        if (actionkind == ActionKind.CREATE) {
            setTitle(getString(R.string.app_title_create_note));
            setContentView(R.layout.activity_create_note);

            txtName = findViewById(R.id.noteNameCreateInput);
            txtDescription = findViewById(R.id.noteDescriptionCreateInput);
        } else if (actionkind == ActionKind.EDIT) {
            setContentView(R.layout.activity_edit_note);

            // Note that the user wants to edit:
            selectedNote = intent.getExtras().getParcelable("selectedNote");
            // Document ID in the database (to know which document to edit):
            noteDocumentId = intent.getStringExtra("noteDocumentId");

            setTitle(getString(R.string.app_title_edit_text) + " '" + selectedNote.getName() + "'");

            txtName = findViewById(R.id.noteNameEditInput);
            txtDescription = findViewById(R.id.noteDescriptionEditInput);

            txtName.setText(selectedNote.getName());
            txtDescription.setText(selectedNote.getDescription());
        } else if (actionkind == ActionKind.SHOW) {
            setTitle(getString(R.string.app_title_note));
            setContentView(R.layout.activity_show_note);

            selectedNote = intent.getExtras().getParcelable("selectedNote");
            // The document ID (note´s) in the database must be passed here because the user can click "Edit" and then need to pass this value to the note editing activity (to update the document in the database):
            noteDocumentId = intent.getStringExtra("noteDocumentId");

            final TextView tvName = findViewById(R.id.asnNoteName);
            final TextView tvDescription = findViewById(R.id.asnNoteDescription);
            final TextView tvCreatedAt = findViewById(R.id.asnNoteCreatedAt);
            final TextView tvLastEditAt = findViewById(R.id.asnNoteLastEditAt);

            tvName.setText(selectedNote.getName());
            tvDescription.setText(selectedNote.getDescription());
            final String createdAtText = getString(R.string.act_note_created_at) + " " + TextHelper.getTextFromTimeStamp(selectedNote.getCreatedAt());
            tvCreatedAt.setText(createdAtText);
            final String lastEditAtText = getString(R.string.act_note_last_edit_at) + " " + TextHelper.getTextFromTimeStamp(selectedNote.getLastEditAt());
            tvLastEditAt.setText(lastEditAtText);
        }

        textValidation = new TextValidationImpl();

        noteDatabase = new NoteDatabaseImpl();
    }

    /**
     * It is called when you click the button to create a new note in activity_create_note.xml.
     *
     * <i>The data entered in the text boxes is tested and if the data is valid, a new topic is
     * created and inserted into the database.</i>
     *
     * @param view
     */
    public void onBtnCreateNoteClick(View view) {
        final boolean validatedBoardName = validateNoteName();
        if (!validatedBoardName) {
            Log.w("CREATE NEW NOTE", "Name is not valid.");
            return;
        }

        Log.w("CREATE NEW NOTE", "Name and description is valid, continuing to create new note and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(txtName);
        final String description = textValidation.getTextFromEditText(txtDescription);

        // Create new Note (add new record to DB):
        final Note note = new Note(topicDocumentId, name, description);
        noteDatabase.add(note, this, createAddNoteSuccessListener());
    }

    /**
     * It is called when you click the button to edit an existing note in activity_edit_note.xml.
     *
     * <i>If the note name is valid, it is updated in the database.</i>
     *
     * @param view
     */
    public void onBtnEditNoteClick(View view) {
        final boolean validatedBoardName = validateNoteName();
        if (!validatedBoardName) {
            Log.w("UPDATE NOTE", "Name is not valid.");
            return;
        }

        Log.w("UPDATE NOTE", "Name and description is valid, continuing to update existing note and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(txtName);
        final String description = textValidation.getTextFromEditText(txtDescription);

        selectedNote.setName(name);
        selectedNote.setDescription(description);

        noteDatabase.update(noteDocumentId, selectedNote, this, createEditNoteSuccessListener());
    }

    /**
     * Validation of the note name entered to txtName.
     *
     * @return true if the name is valid, otherwise false.
     */
    private boolean validateNoteName() {
        // Error texts for invalidate values:
        final String emptyName = getString(R.string.act_note_empty_name);
        final String invalidMinLengthName = getString(R.string.act_note_invalid_min_length_name);
        final String invalidMaxLengthName = getString(R.string.act_note_invalid_max_length_name);

        final boolean validatedName = textValidation.validateBasicTextField(txtName, emptyName, invalidMinLengthName, invalidMaxLengthName);

        if (!validatedName) {
            Log.w("NOTE NAME LENGTH", "Invalid note name length in text field.");
            return false;
        }

        Log.w("NOTE NAME LENGTH", "Valid note name length in text field.");
        return true;
    }

    /**
     * If the note is successfully created, the {@link TopicActivity} activity will start with the
     * selected topic and its all notes overview.
     *
     * @return a listener performing an action if the note is successfully created (runs an activity
     * with selected topic and all its notes overview).
     */
    private OnSuccessListener<DocumentReference> createAddNoteSuccessListener() {
        return new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.w("ADD NOTE SUCCESS", "Note successfully created / added to DB.");

                ToastHelper.showToast(NoteActivity.this, getString(R.string.act_note_add_note_success));

                startTopicIntent();
            }
        };
    }

    /**
     * If the note is successfully updated, the {@link TopicActivity} will start with selected topic
     * and all its notes (include just edited topic) overview.
     *
     * @return a listener performing an action if the note is successfully updated (runs an activity
     * with all notes in selected topic overview).
     */
    private OnSuccessListener<Void> createEditNoteSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE NOTE SUCCESS", "Note successfully updated in DB.");

                ToastHelper.showToast(NoteActivity.this, getString(R.string.act_note_update_note_success));

                startTopicIntent();
            }
        };
    }

    /**
     * {@link TopicActivity} starts with an overview of all the notes belonging to the selected
     * topic.
     */
    private void startTopicIntent() {
        Log.w("START TOPIC ACTIVITY", "Start TopicActivity for show selected topic (and its notes).");

        final Intent topicIntent = new Intent(this, TopicActivity.class);

        topicIntent.putExtra("actionKind", ActionKind.SHOW);
        topicIntent.putExtra("boardDocumentId", boardDocumentId);
        topicIntent.putExtra("selectedBoard", selectedBoard);
        topicIntent.putExtra("topicDocumentId", topicDocumentId);
        topicIntent.putExtra("selectedTopic", selectedTopic);

        startActivity(topicIntent);
        finish();
    }

    /**
     * The method will only be available if the "activity_show_note.xml" context is displayed
     * (representation of one selected note [ActionKind]).
     *
     * <i>Runs the activity for editing the marked note and passes the necessary data (selected
     * note, note document id in the database, etc.).</i>
     *
     * @param view
     */
    public void asnBtnEditNoteClick(View view) {
        Log.w("EDIT SELECTED NOTE", "Start NoteActivity for edit selected note.");

        final Intent noteIntent = new Intent(this, NoteActivity.class);

        noteIntent.putExtra("actionKind", ActionKind.EDIT);
        noteIntent.putExtra("selectedBoard", selectedBoard);
        noteIntent.putExtra("boardDocumentId", boardDocumentId);
        noteIntent.putExtra("selectedTopic", selectedTopic);
        noteIntent.putExtra("topicDocumentId", topicDocumentId);
        noteIntent.putExtra("selectedNote", selectedNote);
        noteIntent.putExtra("noteDocumentId", noteDocumentId);

        startActivity(noteIntent);
        finish();
    }

    /**
     * The method will only be available if the "activity_show_note.xml" context is displayed
     * (representation of one selected note [ActionKind.SHOW]).
     *
     * <i>Deletes the marked note from the database and launches the {@link TopicActivity} with an
     * overview of the selected topics and all its notes.</i>
     *
     * @param view
     */
    public void asnBtnDeleteNoteClick(View view) {
        Log.w("DELETE SELECTED NOTE", "Delete selected note from database.");

        // Delete selected note from database:
        noteDatabase.deleteByDocumentId(noteDocumentId, this);

        startTopicIntent();
    }
}