package cz.uhk.umte.kruncja1.notes.note.fragment;

import cz.uhk.umte.kruncja1.notes.database.entity.Note;

/**
 * Methods to display data about one selected note or to edit a selected note.
 */

public interface OnItemNoteClick {

    /**
     * The activity for editing the selected note opens.
     *
     * @param noteDocumentId
     *         id document (note) in database
     * @param note
     *         note that the user wants to edit (passes to activity)
     */
    void onEditNoteClick(final String noteDocumentId, final Note note);

    /**
     * The activity to display information about the selected note opens.
     *
     * @param noteDocumentId
     *         id document (note) in database
     * @param note
     *         note to be passed to the activity to display its data for user
     */
    void onNoteClick(final String noteDocumentId, final Note note);

    /**
     * Changes the value of the 'done' attribute of a note with the document id (noteDocumentId) in
     * the database. That is. The value of whether a note is met or not is changed.
     *
     * @param noteDocumentId
     *         id document (note) in database
     * @param done
     *         true if the note (/ task) is done, otherwise false.
     */
    void onChcbDoneClick(String noteDocumentId, boolean done);
}