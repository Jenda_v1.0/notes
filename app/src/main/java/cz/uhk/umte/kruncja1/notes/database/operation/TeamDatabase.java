package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import cz.uhk.umte.kruncja1.notes.database.entity.Team;

/**
 * Database operations with teams.
 *
 * <i>The Firebase database can be empty; after the first entity is inserted, the necessary
 * structure is created.</i>
 */

public interface TeamDatabase {

    /**
     * Obtaining all users in the database and according to the listener completeListener and their
     * subsequent processing (conversion to emails, etc.)
     *
     * @param completeListener
     *         listener who handles data processing as needed (for example, saving users to memory,
     *         converting them to emails, etc.)
     */
    void getAllUsersInCollection(OnCompleteListener<QuerySnapshot> completeListener);

    /**
     * Create a new team in the database (create a new record in the database).
     *
     * @param team
     *         new team to create (insert as a new record in the database).
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully create a DB record, which then displays all teams where the
     *         signed in user is a member.
     */
    void add(Team team, final Context context, OnSuccessListener<DocumentReference> successListener);

    /**
     * Updating an existing team in the database.
     *
     * @param documentId
     *         document id / existing team in the database to be updated.
     * @param team
     *         team with new data.
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully update a DB record, which then displays all teams where the
     *         signed in user is a member.
     */
    void update(String documentId, final Team team, final Context context, OnSuccessListener<Void> successListener);

    /**
     * Get all the teams where the signed in user is a member.
     *
     * @param userDocumentUid
     *         the UID of the document (user) in the database that will be searched for by the team
     *         will know if the user with the appropriate uid is a member of a team.
     * @param completeListener
     *         listener that stores the loaded teams (where is user with userDocumentUid member) in
     *         the phone memory.
     */
    void getBySignedUserDocumentUid(String userDocumentUid, OnCompleteListener<QuerySnapshot> completeListener);

    /**
     * Deleting the team and all data related to it from the database.
     *
     * @param teamUid
     *         document / record id in the database to be deleted.
     * @param boardsUid
     *         a list of boards (id of their documents in the database) belonging to the team to be
     *         deleted, along with the team will also be deleted all boards in that list
     *         (boardsUid).
     * @param context
     *         is needed to display success / failure reports (Toast).
     */
    void deleteByDocumentId(String teamUid, List<String> boardsUid, final Context context);

    /**
     * Removing a signed in user (in application) from the team (team).
     *
     * <i><Once a user is removed as a team member, it is determined whether the team has at least
     * one member, if not, it will be removed with all "own" boards.</i>
     *
     * @param teamDocumentId
     *         document id / existing team in the database to be updated.
     * @param userDocumentId
     *         document / record id in the database of signed in user (user with this id will be
     *         removed from the team).
     * @param team
     *         team with new data.
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully update / delete a DB record, which then displays all teams
     *         where the signed in user is a member.
     */
    void leave(String teamDocumentId, String userDocumentId, final Team team, final Context context, OnSuccessListener<Void> successListener);

    /**
     * Remove a user with userDocumentUid from all teams that is a member.
     *
     * <i>If a user deletes his account, the user also must be removed from all teams that user was
     * member of.</i>
     *
     * @param userDocumentId
     *         document (user) id in the database to be removed from all teams where it is located.
     * @param context
     *         is needed to display success / failure reports (Toast).
     */
    void deleteUserFromAllTeams(String userDocumentId, final Context context);
}