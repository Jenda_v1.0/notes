package cz.uhk.umte.kruncja1.notes.topic.fragment;

import cz.uhk.umte.kruncja1.notes.database.entity.Topic;

/**
 * Methods to display data about one selected topic or to edit a selected topic.
 */

public interface OnItemTopicClick {

    /**
     * The activity for editing the selected topic opens.
     *
     * @param topicDocumentId
     *         id document (topic) in database
     * @param topic
     *         topic that the user wants to edit (passes to activity)
     */
    void onEditTopicClick(final String topicDocumentId, final Topic topic);

    /**
     * The activity to display information about the selected topic opens.
     *
     * @param topicDocumentId
     *         id document (topic) in database
     * @param topic
     *         topic to be passed to the activity to display its data for user
     */
    void onTopicClick(final String topicDocumentId, final Topic topic);
}