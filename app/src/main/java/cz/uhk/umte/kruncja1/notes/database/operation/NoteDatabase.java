package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.database.entity.Note;

/**
 * Database operations with notes.
 *
 * <i>The Firebase database can be empty; after the first entity is inserted, the necessary
 * structure is created.</i>
 */

public interface NoteDatabase {

    /**
     * Create a new note in the database (create a new record in the database).
     *
     * @param note
     *         new note to create (insert as a new record in the database).
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully create a DB record, which then displays all notes belonging
     *         to the selected topic.
     */
    void add(Note note, final Context context, OnSuccessListener<DocumentReference> successListener);

    /**
     * Updating an existing note in the database.
     *
     * @param documentId
     *         document id / existing note in the database to be updated.
     * @param note
     *         note with new data.
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully update a DB record, which then displays all notes belonging
     *         to the selected topic.
     */
    void update(String documentId, final Note note, final Context context, OnSuccessListener<Void> successListener);

    /**
     * A change is made to the database as to whether the note is done or not. That is. the 'note'
     * attribute changes to a value in done.
     *
     * @param documentId
     *         document id / existing note in the database to be updated.
     * @param done
     *         true if the note (/ task) is done, otherwise false.
     * @param context
     *         is needed to display failure reports (Toast), success is listed only in the log.
     */
    void updateDoneAttribute(final String documentId, boolean done, final Context context);

    /**
     * Obtain all notes belonging to the selected topic (topic with document id in topicUid).
     *
     * @param topicUid
     *         the ID of the document in the database of the signed in user whose notes are to be
     *         loaded.
     * @param completeListener
     *         listener, which is used to store the notes into the application memory and to report
     *         info about success / failure.
     */
    void getByTopicUid(String topicUid, OnCompleteListener<QuerySnapshot> completeListener);

    /**
     * Deleting the note from the database.
     *
     * @param noteUid
     *         document / record id in the database to be deleted.
     * @param context
     *         is needed to display success / failure reports (Toast).
     */
    void deleteByDocumentId(final String noteUid, final Context context);
}