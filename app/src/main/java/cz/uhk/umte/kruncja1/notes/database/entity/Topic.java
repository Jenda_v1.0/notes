package cz.uhk.umte.kruncja1.notes.database.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Each topic relates to a specific board. The topic represents the "category" to which the notes
 * refer.
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor // This constructor is needed for FireStore
public class Topic extends AbstractEntity {

    /**
     * Unique ID of the specific board (document in the database) to which the topic relates.
     */
    private String boardUid;

    public Topic(String boardUid, String name, String description) {
        super(name, description);
        this.boardUid = boardUid;
    }

    public Topic(String boardUid, String name, String description, Timestamp createdAt, Timestamp lastEditAt) {
        super(name, description, createdAt, lastEditAt);
        this.boardUid = boardUid;
    }

    /**
     * Creates the {@link Topic} object from the data (attribute values) that are on the map.
     *
     * @param data
     *         attributes values.
     *
     * @return cz.uhk.umte.kruncja1.notes.database.entity.Topic instance containing values from the
     * data map.
     */
    public static Topic getTopicFromMap(Map<String, Object> data) {
        final String boardUid = (String) data.get("boardUid");
        final String name = (String) data.get("name");
        final String description = (String) data.get("description");
        final Timestamp createdAt = (Timestamp) data.get("createdAt");
        final Timestamp lastEditAt = (Timestamp) data.get("lastEditAt");

        return new Topic(boardUid, name, description, createdAt, lastEditAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // This is needed (done) when passing an instance of this class to Intent, so you need to "write" all values ​​(serialization replacement because TimeStamp is not serializable):
        dest.writeString(boardUid);
        dest.writeString(name);
        dest.writeString(description);

        dest.writeInt(createdAt.getNanoseconds());
        dest.writeLong(createdAt.getSeconds());

        dest.writeInt(lastEditAt.getNanoseconds());
        dest.writeLong(lastEditAt.getSeconds());
    }

    public static final Parcelable.Creator<Topic> CREATOR = new Parcelable.Creator<Topic>() {
        public Topic createFromParcel(Parcel in) {
            return new Topic(in);
        }

        public Topic[] newArray(int size) {
            return new Topic[size];
        }
    };

    /**
     * The constructor is called when data is obtained from Intent and specific instances of this
     * class is created with the relevant data.
     *
     * @param in
     */
    private Topic(Parcel in) {
        boardUid = in.readString();
        name = in.readString();
        description = in.readString();

        final int iCreatedAt = in.readInt();
        final long lCreatedAt = in.readLong();
        createdAt = new Timestamp(lCreatedAt, iCreatedAt);

        final int iLastEditAt = in.readInt();
        final long lLastEditAt = in.readLong();
        lastEditAt = new Timestamp(lLastEditAt, iLastEditAt);
    }
}