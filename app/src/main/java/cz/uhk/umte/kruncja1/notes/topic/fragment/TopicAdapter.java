package cz.uhk.umte.kruncja1.notes.topic.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;
import cz.uhk.umte.kruncja1.notes.database.operation.TopicDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TopicDatabaseImpl;

/**
 * Used for "data management". In the constructor it gets all the data (among other things) and as
 * the user scrolls the content, the individual items are redrawn quickly and only the data to be
 * displayed is transferred to the displayed parts - what is solved here -> what data are currently
 * displayed.
 */

public class TopicAdapter extends RecyclerView.Adapter<TopicViewHolder> {

    private final Map<String, Topic> topics;

    private final OnItemTopicClick onItemTopicClick;

    private final TopicDatabase topicDatabase;

    TopicAdapter(Map<String, Topic> topics, OnItemTopicClick onItemTopicClick) {
        this.topics = topics;
        this.onItemTopicClick = onItemTopicClick;

        topicDatabase = new TopicDatabaseImpl();
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.fragment_topic_item, viewGroup, false);
        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder topicViewHolder, final int i) {
        // Obtaining document Id in database:
        final List<String> keys = new ArrayList<>(topics.keySet());
        final String topicDocumentId = keys.get(i);

        final Topic topic = topics.get(topicDocumentId);

        final View.OnClickListener deleteTopicOnClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Removing an item from the map in memory:
                topics.remove(topicDocumentId);

                // Delete from database:
                topicDatabase.deleteByDocumentId(topicDocumentId, v.getContext());

                // To reflect the changes in the recycler view:
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, topics.size());
            }
        };

        topicViewHolder.setTopicData(topicDocumentId, topic, deleteTopicOnClickListener, onItemTopicClick);
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }
}