package cz.uhk.umte.kruncja1.notes.topic;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.activity.ActionKind;
import cz.uhk.umte.kruncja1.notes.board.BoardActivity;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.entity.Note;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;
import cz.uhk.umte.kruncja1.notes.database.operation.NoteDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.NoteDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.database.operation.TopicDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TopicDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.note.NoteActivity;
import cz.uhk.umte.kruncja1.notes.note.fragment.NoteFragment;
import cz.uhk.umte.kruncja1.notes.note.fragment.OnItemNoteClick;
import cz.uhk.umte.kruncja1.notes.texthelper.TextHelper;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Displays all topics of the user selected board. Functions are always available to either create a
 * new topic, edit an existing topic, or display information about the selected topic.
 */

public class TopicActivity extends AppCompatActivity implements OnItemNoteClick {

    private EditText txtName, txtDescription;

    /**
     * Board ID (document in database) chosen by the user and to which data (topics) should be
     * displayed, new topic created, modified etc.
     */
    private String boardUid;

    /**
     * In this activity, there is need to maintain / retain the selected board in order to launch
     * {@link BoardActivity} to display the selected board with all topics.
     */
    private Board selectedBoard;

    private TopicDatabase topicDatabase;

    private NoteDatabase noteDatabase;

    private TextValidation textValidation;

    /**
     * The selected topic that the user wants to edit or view its data (content).
     */
    private Topic selectedTopic;

    /**
     * The document ID (topic) in the database that the user wants to edit or view its data.
     */
    private String topicDocumentId;

    /**
     * All notes related to the selected topic.
     */
    private Map<String, Note> notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Displaying activity content based on whether it is a creating new topic or editing an existing topic or show data of selected topic:
        final Intent intent = getIntent();
        selectedBoard = intent.getExtras().getParcelable("selectedBoard");
        final ActionKind actionkind = (ActionKind) intent.getSerializableExtra("actionKind");

        if (actionkind == ActionKind.CREATE) {
            setTitle(R.string.app_title_create_topic);
            setContentView(R.layout.activity_create_topic);

            txtName = findViewById(R.id.topicNameCreateInput);
            txtDescription = findViewById(R.id.topicDescriptionCreateInput);

            boardUid = intent.getStringExtra("boardDocumentId");
        } else if (actionkind == ActionKind.EDIT) {
            setContentView(R.layout.activity_edit_topic);

            // Topic that the user wants to edit.
            selectedTopic = intent.getExtras().getParcelable("selectedTopic");
            // ID of the selected topic in the database:
            topicDocumentId = intent.getStringExtra("topicDocumentId");
            // There is need to be able to start BoardActivity after editing the topic and display all the topics by board uid.
            boardUid = intent.getStringExtra("boardDocumentId");

            setTitle(getString(R.string.app_title_edit_text) + " '" + selectedTopic.getName() + "'");

            txtName = findViewById(R.id.topicNameEditInput);
            txtDescription = findViewById(R.id.topicDescriptionEditInput);

            txtName.setText(selectedTopic.getName());
            txtDescription.setText(selectedTopic.getDescription());
        } else if (actionkind == ActionKind.SHOW) {// only "else" is enough
            setTitle(getString(R.string.app_title_topic));
            setContentView(R.layout.activity_show_topic);

            // Topic that the user wants to show (its data).
            selectedTopic = intent.getExtras().getParcelable("selectedTopic");
            // ID of the selected topic in the database:
            topicDocumentId = intent.getStringExtra("topicDocumentId");
            // There is need to be able to start TopicActivity for editing selected topic, for editing is needed document board id in database:
            boardUid = intent.getStringExtra("boardDocumentId");

            final TextView tvName = findViewById(R.id.astTopicName);
            final TextView tvDescription = findViewById(R.id.astTopicDescription);
            final TextView tvCreatedAt = findViewById(R.id.astTopicCreatedAt);
            final TextView tvLastEditAt = findViewById(R.id.astTopicLastEditAt);

            tvName.setText(selectedTopic.getName());
            tvDescription.setText(selectedTopic.getDescription());
            final String createdAtText = getString(R.string.act_topic_topic_created_at) + " " + TextHelper.getTextFromTimeStamp(selectedTopic.getCreatedAt());
            tvCreatedAt.setText(createdAtText);
            final String lastEditAtText = getString(R.string.act_topic_topic_last_edit_at) + " " + TextHelper.getTextFromTimeStamp(selectedTopic.getLastEditAt());
            tvLastEditAt.setText(lastEditAtText);

            noteDatabase = new NoteDatabaseImpl();

            // Loading notes belonging to the selected topic to display:
            notes = new LinkedHashMap<>();

            final NoteDatabase noteDatabase = new NoteDatabaseImpl();
            noteDatabase.getByTopicUid(topicDocumentId, new GetNotesBelongingToSelectedTopicUidOnCompleteListener());
        }

        textValidation = new TextValidationImpl();

        topicDatabase = new TopicDatabaseImpl();
    }

    /**
     * It is called when you click the button to create a new topic in activity_create_topic.xml.
     *
     * <i>The data entered in the text boxes is tested and if the data is valid, a new topic is
     * created and inserted into the database.</i>
     *
     * @param view
     */
    public void onBtnCreateTopicClick(View view) {
        final boolean validatedBoardName = validateTopicName();
        if (!validatedBoardName) {
            Log.w("CREATE NEW TOPIC", "Name is not valid.");
            return;
        }

        Log.w("CREATE NEW TOPIC", "Name and description is valid, continuing to create new topic and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(txtName);
        final String description = textValidation.getTextFromEditText(txtDescription);

        // Create new Topic (add new record to DB):
        final Topic topic = new Topic(boardUid, name, description);
        topicDatabase.add(topic, this, createAddTopicSuccessListener());
    }

    /**
     * It is called when you click the button to edit an existing board in activity_edit_topic.xml.
     *
     * <i>If the topic name is valid, it is updated in the database.</i>
     *
     * @param view
     */
    public void onBtnEditTopicClick(View view) {
        final boolean validatedBoardName = validateTopicName();
        if (!validatedBoardName) {
            Log.w("UPDATE TOPIC", "Name is not valid.");
            return;
        }

        Log.w("UPDATE TOPIC", "Name and description is valid, continuing to update existing board and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(txtName);
        final String description = textValidation.getTextFromEditText(txtDescription);

        selectedTopic.setName(name);
        selectedTopic.setDescription(description);

        topicDatabase.update(topicDocumentId, selectedTopic, this, createEditTopicSuccessListener());
    }

    /**
     * Validation of the topic name entered to txtName.
     *
     * @return true if the name is valid, otherwise false.
     */
    private boolean validateTopicName() {
        // Error texts for invalidate values:
        final String emptyName = getString(R.string.act_topic_empty_name);
        final String invalidMinLengthName = getString(R.string.act_topic_invalid_min_length_name);
        final String invalidMaxLengthName = getString(R.string.act_topic_invalid_max_length_name);

        final boolean validatedName = textValidation.validateBasicTextField(txtName, emptyName, invalidMinLengthName, invalidMaxLengthName);

        if (!validatedName) {
            Log.w("TOPIC NAME LENGTH", "Invalid topic name length in text field.");
            return false;
        }

        Log.w("TOPIC NAME LENGTH", "Valid topic name length in text field.");
        return true;
    }

    /**
     * If the topic is successfully created, the {@link BoardActivity} activity will start with the
     * selected board and its all topics overview.
     *
     * @return a listener performing an action if the topic is successfully created (runs an
     * activity with selected board and all its topics overview).
     */
    private OnSuccessListener<DocumentReference> createAddTopicSuccessListener() {
        return new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.w("ADD TOPIC SUCCESS", "Topic successfully created / added to DB.");

                ToastHelper.showToast(TopicActivity.this, getString(R.string.act_topic_add_topic_success));

                startBoardIntent();
            }
        };
    }

    /**
     * If the topic is successfully updated, the {@link BoardActivity} will start with selected
     * board and all its topics (include just edited topic) overview.
     *
     * @return a listener performing an action if the topic is successfully updated (runs an
     * activity with all topics in selected board overview).
     */
    private OnSuccessListener<Void> createEditTopicSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE TOPIC SUCCESS", "Topic successfully updated in DB.");

                ToastHelper.showToast(TopicActivity.this, getString(R.string.act_topic_update_topic_success));

                startBoardIntent();
            }
        };
    }

    private void startBoardIntent() {
        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.SHOW);
        boardIntent.putExtra("board", selectedBoard);
        boardIntent.putExtra("documentId", boardUid);

        startActivity(boardIntent);
        finish();
    }

    /**
     * The method will only be available if the "activity_show_topic.xml" context is displayed
     * (representation of one selected topic [ActionKind.SHOW]).
     *
     * <i>Deletes the marked topic from the database and launches the {@link BoardActivity} with an
     * overview of the selected boards and all its topics.</i>
     *
     * @param view
     */
    public void astBtnDeleteTopicClick(View view) {
        Log.w("DELETE SELECTED TOPIC", "Delete selected topic from database.");

        // Delete selected topic from database:
        topicDatabase.deleteByDocumentId(topicDocumentId, this);

        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.SHOW);
        boardIntent.putExtra("board", selectedBoard);
        boardIntent.putExtra("documentId", boardUid);

        startActivity(boardIntent);
        finish();
    }

    /**
     * The method will only be available if the "activity_show_topic.xml" context is displayed
     * (representation of one selected topic [ActionKind.SHOW]).
     *
     * <i>Runs the activity for editing the marked topic and passes the necessary data (selected
     * board, board document id in the database, etc.).</i>
     *
     * @param view
     */
    public void astBtnEditTopicClick(View view) {
        Log.w("EDIT SELECTED TOPIC", "Start TopicActivity for edit selected topic.");

        final Intent topicIntent = new Intent(this, TopicActivity.class);

        topicIntent.putExtra("actionKind", ActionKind.EDIT);
        topicIntent.putExtra("selectedBoard", selectedBoard);
        topicIntent.putExtra("boardDocumentId", boardUid);
        topicIntent.putExtra("selectedTopic", selectedTopic);
        topicIntent.putExtra("topicDocumentId", topicDocumentId);

        startActivity(topicIntent);
        finish();
    }

    @Override
    public void onEditNoteClick(String noteDocumentId, Note note) {
        Log.w("EDIT SELECTED NOTE", "Start NoteActivity for edit selected note.");
        startNoteActivity(ActionKind.EDIT, noteDocumentId, note);
    }

    @Override
    public void onNoteClick(String noteDocumentId, Note note) {
        Log.w("OPEN NOTE OVERVIEW", "Start NoteActivity for show selected note overview.");
        startNoteActivity(ActionKind.SHOW, noteDocumentId, note);
    }

    @Override
    public void onChcbDoneClick(String noteDocumentId, boolean done) {
        Log.w("UPDATE DONE ATTRIBUTE", "Update 'done' attribute in note with document uid: " + noteDocumentId);
        noteDatabase.updateDoneAttribute(noteDocumentId, done, this);
    }

    private void startNoteActivity(ActionKind actionKind, String noteDocumentId, Note note) {
        final Intent noteIntent = new Intent(this, NoteActivity.class);

        noteIntent.putExtra("actionKind", actionKind);
        noteIntent.putExtra("selectedBoard", selectedBoard);
        noteIntent.putExtra("boardDocumentId", boardUid);
        noteIntent.putExtra("selectedTopic", selectedTopic);
        noteIntent.putExtra("topicDocumentId", topicDocumentId);
        noteIntent.putExtra("selectedNote", note);
        noteIntent.putExtra("noteDocumentId", noteDocumentId);

        startActivity(noteIntent);
        finish();
    }

    public void astBtnCreateNoteClick(View view) {
        Log.w("CREATE NEW NOTE", "Start NoteActivity for create a new note.");

        final Intent noteIntent = new Intent(this, NoteActivity.class);

        noteIntent.putExtra("actionKind", ActionKind.CREATE);
        noteIntent.putExtra("selectedBoard", selectedBoard);
        noteIntent.putExtra("boardDocumentId", boardUid);
        noteIntent.putExtra("selectedTopic", selectedTopic);
        noteIntent.putExtra("topicDocumentId", topicDocumentId);

        startActivity(noteIntent);
        finish();
    }

    /**
     * Listener, which is used to load all notes of the related to selected topic in selected board
     * of signed in user. Loaded notes will be stored in the notes collection. Next, the relevant
     * notes will appear in the activity content, etc.
     */
    private class GetNotesBelongingToSelectedTopicUidOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET NOTE BY TOPIC UID", "Note data was successfully obtained: " + document.getData());

                    final Note noteFromMap = Note.getNoteFromMap(document.getData());
                    notes.put(document.getId(), noteFromMap);
                    Log.w("GET NOTE BY TOPIC UID", "Obtained note successfully created: " + noteFromMap);
                }

                Log.w("GET NOTES BY TOPIC UID", "All successfully obtained notes: " + notes);
                // "Create" a fragment in which individual topics will show:
                final NoteFragment noteFragment = (NoteFragment) getSupportFragmentManager().findFragmentById(R.id.noteFragment);
                if (noteFragment != null) {
                    noteFragment.setData(notes, TopicActivity.this);
                }
            } else {
                Log.w("GET NOTES BY TOPIC UID", "Notes data could not be obtained.");
                ToastHelper.showToast(TopicActivity.this, getString(R.string.act_topic_note_data_not_obtained));
            }
        }
    }
}