package cz.uhk.umte.kruncja1.notes.board.fragment;

import cz.uhk.umte.kruncja1.notes.database.entity.Board;

/**
 * Methods to display data about one selected board or to edit a selected board.
 */

public interface OnItemBoardClick {

    /**
     * The activity for editing the selected board opens.
     *
     * @param boardDocumentId
     *         id document (board) in database
     * @param board
     *         board that the user wants to edit (passes to activity)
     */
    void onEditBoardClick(final String boardDocumentId, final Board board);

    /**
     * The activity to display information about the selected board opens.
     *
     * @param boardDocumentId
     *         id document (board) in database
     * @param board
     *         board to be passed to the activity to display its data for user
     */
    void onBoardClick(final String boardDocumentId, final Board board);
}