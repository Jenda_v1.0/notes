package cz.uhk.umte.kruncja1.notes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.activity.ActionKind;
import cz.uhk.umte.kruncja1.notes.board.BoardActivity;
import cz.uhk.umte.kruncja1.notes.board.fragment.BoardFragment;
import cz.uhk.umte.kruncja1.notes.board.fragment.OnItemBoardClick;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.login.LoginActivity;
import cz.uhk.umte.kruncja1.notes.profile.ProfileActivity;
import cz.uhk.umte.kruncja1.notes.team.TeamActivity;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Main / default activity of signed in user. The signed in user's data is displayed here.
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnItemBoardClick {

    private FirebaseAuth firebaseAuth;

    /**
     * Signed in user.
     */
    private User user;
    /**
     * The record ID in the database that contains the signed in user information. This id is needed
     * to delete a user from the database.
     */
    private String userDocumentId;

    private Map<String, Board> boards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_title_boards));
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabCreateBoard = findViewById(R.id.fabCreateBoard);
        fabCreateBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent boardIntent = new Intent(MainActivity.this, BoardActivity.class);

                if (firebaseAuth.getCurrentUser() != null) {// not necessary condition
                    boardIntent.putExtra("userUid", firebaseAuth.getCurrentUser().getUid());
                }

                boardIntent.putExtra("actionKind", ActionKind.CREATE);

                startActivity(boardIntent);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // Retrieving all boards owned by signed in user (LinkedHashMap remain constant order over time):
        boards = new LinkedHashMap<>();

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        // Retrieving a signed in user to work with it and retrieve its data, etc.:
        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            // Obtaining signed in user data for further work:
            final UserDatabase userDatabase = new UserDatabaseImpl();
            userDatabase.get(currentUser.getUid(), new GetUserOnCompleteListener());

            // Until the user verifies the email, his / her boards will not be listed:
            if (currentUser.isEmailVerified()) {
                Log.w("VERIFIED EMAIL", "The user has verified the email, continuing to load his boards.");
                final BoardDatabase boardDatabase = new BoardDatabaseImpl();
                boardDatabase.getByUserUid(currentUser.getUid(), new GetBoardsCreatedByUserUidOnCompleteListener());
            } else {
                Log.w("VERIFIED EMAIL", "User has not verified email.");
                ToastHelper.showToast(this, getString(R.string.act_main_user_has_not_verified_email));
            }
        }
    }

    /**
     * Set the username and email of the signed in user to the drop-down menu.
     */
    private void setSignedUserValuesToNavMenu() {
        Log.w("SET USERNAME AND EMAIL", "Set user name and password to the drop-down menu bar.");

        // Set username and email to menu:
        final NavigationView navigationView = findViewById(R.id.nav_view);

        final TextView tvNavHeaderMainUsername = navigationView.getHeaderView(0).findViewById(R.id.navHeaderMainUsername);
        final TextView tvNavHeaderMainEmail = navigationView.getHeaderView(0).findViewById(R.id.navHeaderMainEmail);

        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser == null) {
            Log.w("SET USERNAME AND EMAIL", "User is not signed in, can not set username and email to TextView.");
            return;
        }

        if (user != null && !user.getUserUid().equals("-1")) {
            tvNavHeaderMainUsername.setText(user.getUsername());
            tvNavHeaderMainEmail.setText(currentUser.getEmail());
            Log.w("SET USERNAME AND EMAIL", "Username and email of the signed in user have been successfully set to the drop-down menu.");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else if (id == R.id.nav_teams) {
            final Intent teamIntent = new Intent(this, TeamActivity.class);

            teamIntent.putExtra("actionKind", ActionKind.SHOW);
            teamIntent.putExtra("userDocumentId", userDocumentId);
            teamIntent.putExtra("signedInUser", user);

            startActivity(teamIntent);
        } else if (id == R.id.nav_profile) {
            final Intent profileIntent = new Intent(this, ProfileActivity.class);

            profileIntent.putExtra("signedInUser", user);
            profileIntent.putExtra("userDocumentId", userDocumentId);

            startActivity(profileIntent);
        } else if (id == R.id.nav_log_out) {
            Log.w("LOG OUT", "Sign out user.");
            firebaseAuth.signOut();

            if (firebaseAuth.getCurrentUser() == null) {
                Log.w("LOG OUT", "User was successfully signed out, redirect to LoginActivity.");

                ToastHelper.showToast(this, getString(R.string.act_main_sign_out_success));

                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onEditBoardClick(final String boardDocumentId, final Board board) {
        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.EDIT);
        boardIntent.putExtra("board", board);
        boardIntent.putExtra("documentId", boardDocumentId);

        startActivity(boardIntent);
    }

    @Override
    public void onBoardClick(String boardDocumentId, Board board) {
        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.SHOW);
        boardIntent.putExtra("board", board);
        boardIntent.putExtra("documentId", boardDocumentId);

        startActivity(boardIntent);
    }

    /**
     * A listener that loads users from a database and stores it in a local variable for further
     * work. Next, set his user name and email to TextView in the pop-up menu.
     *
     * <i>This listener is needed because, after calling the "get" method over the collection, it
     * executes a command to retrieve data from the database, but retrieving data from the database
     * itself is asynchronous, so when it is done, that listener is executed, where the data is
     * processed for further work.</i>
     */
    private class GetUserOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET USER", "User data was successfully obtained: " + document.getData());

                    userDocumentId = document.getId();
                    user = User.getUserFromMap(document.getData());
                    Log.w("GET USER", "Obtained user successfully created, document uid: '" + userDocumentId + "', user: " + user);

                    setSignedUserValuesToNavMenu();

                    ToastHelper.showToast(MainActivity.this, getString(R.string.act_main_user_data_successfully_obtained));
                }
            } else {
                Log.w("GET USER", "User data could not be obtained.");
                ToastHelper.showToast(MainActivity.this, getString(R.string.act_main_user_data_not_obtained));
            }
        }
    }

    /**
     * Listener, which is used to load all boards of the signed in user. Loaded boards will be
     * stored in the boards collection. Next, the relevant boards will appear in the activity
     * content, etc.
     */
    private class GetBoardsCreatedByUserUidOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET BOARD BY USER UID", "Board data was successfully obtained: " + document.getData());

                    final Board boardFromMap = Board.getBoardFromMap(document.getData());
                    boards.put(document.getId(), boardFromMap);
                    Log.w("GET BOARD BY USER UID", "Obtained board successfully created: " + boardFromMap);
                }

                Log.w("GET BOARDS BY USER UID", "All successfully obtained boards: " + boards);
                // "Create" a fragment in which individual boards will show:
                final BoardFragment boardFragment = (BoardFragment) getSupportFragmentManager().findFragmentById(R.id.boardFragment);
                if (boardFragment != null) {
                    boardFragment.setData(boards, MainActivity.this);
                }
            } else {
                Log.w("GET BOARDS BY USER UID", "Boards data could not be obtained.");
                ToastHelper.showToast(MainActivity.this, getString(R.string.act_main_boards_data_not_obtained));
            }
        }
    }
}