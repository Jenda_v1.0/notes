package cz.uhk.umte.kruncja1.notes.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;

import cz.uhk.umte.kruncja1.notes.MainActivity;
import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.register.RegisterActivity;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Registered user login.
 */

public class LoginActivity extends AppCompatActivity {

    private EditText txtEmail, txtPassword;

    private TextValidation textValidation;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_title_login));
        setContentView(R.layout.activity_login);

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        textValidation = new TextValidationImpl();

        txtEmail = findViewById(R.id.emailInput);
        txtPassword = findViewById(R.id.passwordInput);

        // Listener on press Enter key:
        addEnterKeyListener(txtEmail);
        addEnterKeyListener(txtPassword);

        // Check if user is already signed in:
        checkSignedInUser();
    }

    public void onBtnRegisterClick(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    /**
     * User sign in attempt.
     *
     * <i>Get email and password from form, their "basic" validation and sign in attempt.</i>
     */
    private void loginUser() {
        // Error text:
        final String emptyEmail = getString(R.string.act_login_empty_email);
        final String emptyPassword = getString(R.string.act_login_empty_password);

        // Validation results:
        final boolean validatedEmail = textValidation.validateEmptyField(txtEmail, emptyEmail);
        final boolean validatedPassword = textValidation.validateEmptyField(txtPassword, emptyPassword);

        if (!validatedEmail || !validatedPassword) {
            Log.w("LOGIN", "Invalid email or password.");
            return;
        }

        Log.w("LOGIN", "Valid email and password, trying to sign in.");

        // Texts form text fields:
        final String email = textValidation.getTextFromEditText(txtEmail);
        final String password = textValidation.getTextFromEditText(txtPassword);

        // Login:
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.w("LOGIN", "User was signed in successfully, starting MainActivity.");
                            startMainActivity();
                        } else {
                            Log.w("LOGIN", "An error occurred while trying to sign in.");

                            try {
                                throw task.getException();
                            }
                            // if user enters wrong email.
                            catch (FirebaseAuthInvalidUserException invalidEmail) {
                                Log.w("LOGIN", "Incorrect email.");
                                final String incorrectEmail = getString(R.string.act_login_toast_incorrect_email);
                                showToast(incorrectEmail);
                            }
                            // if user enters wrong password.
                            catch (FirebaseAuthInvalidCredentialsException wrongPassword) {
                                Log.w("LOGIN", "Incorrect password.");
                                final String incorrectPassword = getString(R.string.act_login_toast_incorrect_password);
                                showToast(incorrectPassword);
                            } catch (Exception e) {
                                Log.w("LOGIN", e.getMessage());
                                final String incorrectCredentials = getString(R.string.act_login_toast_incorrect_credentials);
                                showToast(incorrectCredentials);
                            }
                        }
                    }
                });
    }

    public void onBtnLoginClick(View view) {
        loginUser();
    }

    /**
     * Adding a listener to the editText text box resolving to press the enter key. In this case, an
     * attempt is made to log in the user according to the entered data in the form.
     *
     * @param editText
     *         a text field where the listener is to be added.
     */
    private void addEnterKeyListener(EditText editText) {
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    loginUser();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Check if the user is already signed in, if so, he will be redirected to his or her home
     * activity ({@link MainActivity}.
     */
    private void checkSignedInUser() {
        Log.w("USER", "Check if user is signed in.");
        if (firebaseAuth.getCurrentUser() != null) {
            Log.w("USER", "User is signed in, starting MainActivity.");
            // User is signed in, co he will be redirected to his home activity:
            startMainActivity();
        }
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void showToast(String text) {
        ToastHelper.showToast(LoginActivity.this, text);
    }
}