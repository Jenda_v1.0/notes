package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Implementation of database operations with topics.
 */

public class TopicDatabaseImpl implements TopicDatabase {

    private static final NoteDatabase NOTE_DATABASE = new NoteDatabaseImpl();

    @Override
    public void add(Topic topic, final Context context, OnSuccessListener<DocumentReference> successListener) {
        Log.w("ADD TOPIC", "Add new topic to DB: " + topic);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.TOPIC.getName())
                .add(topic)
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ADD TOPIC FAILURE", "Failed to create new topic.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_add_topic_failure));
                    }
                });
    }

    @Override
    public void update(String documentId, final Topic topic, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("UPDATE TOPIC", "Update existing topic in DB with id: '" + documentId + "', new topic: " + topic);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        final DocumentReference document = db.collection(DbCollection.TOPIC.getName()).document(documentId);

        document.update("boardUid", topic.getBoardUid());// not necessary
        document.update("name", topic.getName());
        document.update("description", topic.getDescription());
        // (createdAt does not change)
        document.update("lastEditAt", Timestamp.now())
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("UPDATE TOPIC FAILURE", "Failed to update existing topic: " + topic);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_update_topic_failure));
                    }
                });
    }

    @Override
    public void getByBoardUid(String uid, OnCompleteListener<QuerySnapshot> completeListener) {
        Log.w("GET TOPICS BY BOARD UID", "Get topics related to board uid: " + uid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.TOPIC.getName())
                .whereEqualTo("boardUid", uid)
                .get()
                .addOnCompleteListener(completeListener);
    }

    @Override
    public void deleteByDocumentId(final String id, final Context context) {
        Log.w("DELETE TOPIC BY DOC. ID", "Delete topic with its document id: " + id);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // Delete all notes that belong to the topic with id to be deleted:
        db.collection(DbCollection.NOTE.getName())
                .whereEqualTo("topicUid", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.w("GET NOTES BY TOPIC UID", "All notes with topic uid '" + id + "' successfully found.");

                            for (final QueryDocumentSnapshot document : task.getResult()) {
                                Log.w("DELETE NOTE", "Delete note with uid: " + document.getId());
                                // Delete note by its document id:
                                NOTE_DATABASE.deleteByDocumentId(document.getId(), context);
                            }
                        } else {
                            Log.w("GET NOTES BY TOPIC UID", "Failed to get notes with topic uid: " + id);
                            ToastHelper.showToast(context, context.getString(R.string.db_impl_obtain_notes_in_delete_topic_failed));
                        }
                    }
                });

        db.collection(DbCollection.TOPIC.getName())
                .document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("DELETE TOPIC BY DOC. ID", "Topic successfully deleted.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_topic_success));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DELETE TOPIC BY DOC. ID", "An error occurred while trying to delete topic with id: " + id, e);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_topic_failure));
                    }
                });
    }
}