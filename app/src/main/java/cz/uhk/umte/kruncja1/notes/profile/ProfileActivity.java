package cz.uhk.umte.kruncja1.notes.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.login.LoginActivity;
import cz.uhk.umte.kruncja1.notes.texthelper.TextHelper;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * View signed in user data and options with his account (cancel, change password, change data,
 * ...).
 */

public class ProfileActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    /**
     * Signed in user.
     */
    private User signedInUser;
    /**
     * The record ID in the database that contains the signed in user information. This id is needed
     * to delete a user from the database.
     */
    private String userDocumentId;

    private UserDatabase userDatabase;

    private TeamDatabase teamDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_title_profile);
        setContentView(R.layout.activity_profile);

        userDatabase = new UserDatabaseImpl();

        teamDatabase = new TeamDatabaseImpl();

        // It is better to pass the signed in user, otherwise it would need to be reloaded from the database (write listener, etc.):
        signedInUser = getIntent().getExtras().getParcelable("signedInUser");
        userDocumentId = getIntent().getStringExtra("userDocumentId");

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        if (signedInUser != null && !signedInUser.getUserUid().equals("-1")) {
            final EditText etUsername = findViewById(R.id.paEditTextUsername);
            etUsername.setText(signedInUser.getUsername());
            etUsername.setKeyListener(null);

            final EditText etFirstName = findViewById(R.id.paEditTextFirstName);
            etFirstName.setText(signedInUser.getFirstName());
            etFirstName.setKeyListener(null);

            final EditText etLastName = findViewById(R.id.paEditTextLastName);
            etLastName.setText(signedInUser.getLastName());
            etLastName.setKeyListener(null);

            final EditText etEmail = findViewById(R.id.paEditTextEmail);
            etEmail.setText(signedInUser.getEmail());
            etEmail.setKeyListener(null);

            final EditText etTelephoneNumber = findViewById(R.id.paEditTextTelephoneNumber);
            if (signedInUser.getTelephoneNumber() != null && signedInUser.getTelephoneNumber() > -1) {
                final String telephoneNumber = signedInUser.getTelephoneNumber().toString();
                final String formattedNumber = TextHelper.formatTelephoneNumber(telephoneNumber, TextHelper.REG_EX_REPLACEMENT_TELEPHONE_NUMBER_WITH_CODE);
                etTelephoneNumber.setText(formattedNumber);
            }
            etTelephoneNumber.setKeyListener(null);

            final EditText etCreatedAt = findViewById(R.id.paEditTextCreatedAt);
            etCreatedAt.setText(TextHelper.getTextFromTimeStamp(signedInUser.getCreatedAt()));
            etCreatedAt.setKeyListener(null);

            final EditText etLastEditAt = findViewById(R.id.paEditTextLastEditAt);
            etLastEditAt.setText(TextHelper.getTextFromTimeStamp(signedInUser.getLastEditAt()));
            etLastEditAt.setKeyListener(null);
        }
    }

    /**
     * Running activity where the user can edit profile data.
     *
     * @param view
     */
    public void OnBtnChangeData(View view) {
        Log.w("UPDATE PROFILE", "Start UpdateProfileActivity to update profile values.");

        final Intent updateProfileIntent = new Intent(this, UpdateProfileActivity.class);

        updateProfileIntent.putExtra("signedInUser", signedInUser);
        updateProfileIntent.putExtra("userDocumentId", userDocumentId);

        startActivity(updateProfileIntent);
    }

    /**
     * Sending an email to change your password.
     *
     * @param view
     */
    public void OnBtnChangePassword(View view) {
        Log.w("SEND PASSWD RESET EMAIL", "Send email to change password.");

        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if (currentUser != null && currentUser.getEmail() != null) {
            firebaseAuth.sendPasswordResetEmail(currentUser.getEmail())
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.w("SEND PASSWD RESET EMAIL", "Password change request sent successfully.");
                            ToastHelper.showToast(ProfileActivity.this, getString(R.string.act_profile_send_password_change_request_success));
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("SEND PASSWD RESET EMAIL", "There was an error trying to send a password change request.", e);
                            ToastHelper.showToast(ProfileActivity.this, getString(R.string.act_profile_send_password_change_request_failure));
                        }
                    });
        }
    }

    /**
     * Sending verification email.
     *
     * @param view
     */
    public void OnBtnVerifyEmail(View view) {
        Log.w("SEND VERIFICATION EMAIL", "Sending email for verification.");

        // Sending verification email:
        if (firebaseAuth.getCurrentUser() != null) {
            firebaseAuth.getCurrentUser().sendEmailVerification()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.w("SEND VERIFICATION EMAIL", "Verification email was successfully sent.");
                            ToastHelper.showToast(ProfileActivity.this, getString(R.string.act_profile_verification_email_send_success));

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("SEND VERIFICATION EMAIL", "There was an error trying to send a verification email.", e);
                            ToastHelper.showToast(ProfileActivity.this, getString(R.string.act_profile_verification_email_send_failure));
                        }
                    });
        }
    }

    /**
     * Delete user and all his data (boards, topic and notes).
     *
     * @param view
     */
    public void OnBtnDeleteAccount(View view) {
        Log.w("DELETE USER", "Delete user by his UID");

        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if (currentUser != null) {
            Log.w("DELETE USER", "Delete user with UID: " + currentUser.getUid());

            currentUser.delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.w("DELETE USER", "User with document UID: '" + currentUser.getUid() + "' successfully deleted.");
                            ToastHelper.showToast(ProfileActivity.this, getString(R.string.act_profile_delete_user_success));

                            // First, the signed in user is removed from all teams where he / she is a member:
                            teamDatabase.deleteUserFromAllTeams(userDocumentId, ProfileActivity.this);

                            // Delete a user and his data from the database:
                            userDatabase.delete(userDocumentId, currentUser.getUid(), ProfileActivity.this, createDeleteUserSuccessListener());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("DELETE USER", "An error occurred while trying to delete user with UID: '" + currentUser.getUid() + "'.");
                            ToastHelper.showToast(ProfileActivity.this, getString(R.string.act_profile_delete_user_failure));
                        }
                    });
        }
    }

    private OnSuccessListener<Void> createDeleteUserSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                firebaseAuth.signOut();
                Log.w("LOG OUT", "User was successfully signed out, redirect to LoginActivity.");

                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();
            }
        };
    }
}