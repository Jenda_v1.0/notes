package cz.uhk.umte.kruncja1.notes.database.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Represents some information that the user wants to note or something in terms of "task", etc.
 * Each note refers to a specific topic (/ category) in a specific board.
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor // This constructor is needed for FireStore
public class Note extends AbstractEntity {

    /**
     * Unique ID of the particular topic (document in the database) to which the note refers.
     */
    private String topicUid;

    /**
     * Whether a particular note is completed or not.
     */
    private boolean done;

    public Note(String topicUid, String name, String description) {
        super(name, description);
        this.topicUid = topicUid;
    }

    private Note(String topicUid, boolean done, String name, String description, Timestamp createdAt, Timestamp lastEditAt) {
        super(name, description, createdAt, lastEditAt);
        this.topicUid = topicUid;
        this.done = done;
    }

    /**
     * Creates the {@link Note} object from the data (attribute values) that are on the map.
     *
     * @param data
     *         attributes values.
     *
     * @return cz.uhk.umte.kruncja1.notes.database.entity.Note instance containing values from the
     * data map.
     */
    public static Note getNoteFromMap(Map<String, Object> data) {
        final String topicUid = (String) data.get("topicUid");
        final String name = (String) data.get("name");
        final String description = (String) data.get("description");
        final boolean done = (boolean) data.get("done");
        final Timestamp createdAt = (Timestamp) data.get("createdAt");
        final Timestamp lastEditAt = (Timestamp) data.get("lastEditAt");

        return new Note(topicUid, done, name, description, createdAt, lastEditAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // This is needed (done) when passing an instance of this class to Intent, so you need to "write" all values ​​(serialization replacement because TimeStamp is not serializable):
        dest.writeString(topicUid);
        dest.writeString(Boolean.toString(done));
        dest.writeString(name);
        dest.writeString(description);

        dest.writeInt(createdAt.getNanoseconds());
        dest.writeLong(createdAt.getSeconds());

        dest.writeInt(lastEditAt.getNanoseconds());
        dest.writeLong(lastEditAt.getSeconds());
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    /**
     * The constructor is called when data is obtained from Intent and specific instances of this
     * class is created with the relevant data.
     *
     * @param in
     */
    private Note(Parcel in) {
        topicUid = in.readString();
        done = Boolean.parseBoolean(in.readString());
        name = in.readString();
        description = in.readString();

        final int iCreatedAt = in.readInt();
        final long lCreatedAt = in.readLong();
        createdAt = new Timestamp(lCreatedAt, iCreatedAt);

        final int iLastEditAt = in.readInt();
        final long lLastEditAt = in.readLong();
        lastEditAt = new Timestamp(lLastEditAt, iLastEditAt);
    }
}