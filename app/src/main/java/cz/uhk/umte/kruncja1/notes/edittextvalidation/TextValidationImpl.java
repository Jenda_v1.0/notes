package cz.uhk.umte.kruncja1.notes.edittextvalidation;

import android.util.Log;
import android.widget.EditText;

/**
 * Validation of values in text fields (EditText) and auxiliary methods for working with text and
 * text fields (setting errors in text field, etc.)
 */

public class TextValidationImpl implements TextValidation {

    /**
     * Minimum length for texts in required text fields.
     */
    private static final int MINIMAL_LENGTH = 2;
    /**
     * Maximum length for texts in required text fields.
     */
    private static final int MAXIMAL_LENGTH = 255;

    @Override
    public boolean validateEmptyField(EditText editText, String errorText) {
        Log.w("VALIDATE TEXT FIELD", "Check if text field is empty.");

        final String text = getTextFromEditText(editText);

        if (text.isEmpty()) {
            Log.w("VALIDATE TEXT FIELD", "Text field is empty (is not valid).");
            editText.setError(errorText);
            return false;
        }

        Log.w("VALIDATE TEXT FIELD", "Text field is valid (is not empty).");
        return true;
    }

    @Override
    public boolean validateBasicTextField(EditText editText, String errorEmptyText, String errorMinLengthText, String errorMaxLengthText) {
        Log.w("VALIDATE TEXT FIELD", "Check if text field is not empty and all texts are greater than or equal to minimum characters.");

        if (!validateEmptyField(editText, errorEmptyText)) {
            Log.w("VALIDATE TEXT FIELD", "Text field is not valid (is empty).");
            return false;
        }

        final String text = getTextFromEditText(editText);

        if (text.length() < MINIMAL_LENGTH) {
            Log.w("VALIDATE TEXT FIELD", "Text is less then required length.");
            editText.setError(errorMinLengthText);
            return false;
        }

        if (text.length() > MAXIMAL_LENGTH) {
            Log.w("VALIDATE TEXT FIELD", "Text is greater then required length.");
            editText.setError(errorMaxLengthText);
            return false;
        }

        Log.w("VALIDATE TEXT FIELD", "Text is valid (is not empty and has size greater the required minimum).");
        return true;
    }

    @Override
    public String getTextFromEditText(EditText editText) {
        Log.w("TEXT FIELD", "Get text from text field.");
        return editText.getText().toString().trim();
    }
}