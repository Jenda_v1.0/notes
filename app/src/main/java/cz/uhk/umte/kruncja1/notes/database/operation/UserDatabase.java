package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.database.entity.User;

/**
 * Database operations with users.
 *
 * <i>The Firebase database can be empty; after the first entity is inserted, the necessary
 * structure is created.</i>
 */

public interface UserDatabase {

    /**
     * Add new user to database.
     *
     * <i>If no entity is yet in the database, a new one will be created (no default configuration
     * on the firebase firestore side is needed.</i>
     *
     * @param user
     *         new user to be inserted into the database.
     * @param context
     *         context that is needed to inform the user of the result of the operation (Toast).
     */
    void add(User user, final Context context);

    /**
     * Obtain a user from the database.
     *
     * <i>It would be possible to give the user's uid identifier (see code samples in method
     * bodies). But each User contains a userUid, which is the user id generated when a new user
     * with email and password is created (to identify which user the data relates to). According to
     * this uid, the logged in user is searched.</i>
     *
     * @param uid
     *         user identifier (signed in) to find the database record.
     * @param completeListener
     *         listener that stores the acquired user (record from database) into a local variable.
     *         Because getting an item from a database is an asynchronous call, you need to respond
     *         "separately" to retrieve an item from a database (for storage, etc.).
     */
    void get(String uid, OnCompleteListener<QuerySnapshot> completeListener);

    /**
     * Delete a user with all his data.
     *
     * @param userDocumentId
     *         the user ID (document in the database) to be deleted.
     * @param context
     *         context that is needed to inform the user of the result of the operation (Toast).
     * @param successListener
     *         listener, which is performed if the user record is successfully deleted from the
     *         database. The listener then sign out the user and starts the login activity.
     */
    void delete(final String userDocumentId, String userUid, final Context context, OnSuccessListener<Void> successListener);

    /**
     * Updating an existing user in the database.
     *
     * @param userDocumentId
     *         document id / existing user in the database to be updated.
     * @param user
     *         user with new data
     * @param context
     *         is needed to displays success / failure reports (Toast).
     */
    void update(String userDocumentId, final User user, final Context context);

    /**
     * Updating an existing user in the database.
     *
     * @param userDocumentId
     *         document id / existing user in the database to be updated.
     * @param user
     *         user with new data
     * @param context
     *         is needed to displays success / failure reports (Toast).
     * @param successListener
     *         listener to successfully update a DB record, which then starts {@link
     *         cz.uhk.umte.kruncja1.notes.profile.ProfileActivity} and show actual (edited) profile
     *         data.
     */
    void update(String userDocumentId, final User user, final Context context, OnSuccessListener<Void> successListener);

    /**
     * Transferring a phone number to a form suitable for storage in a database (in the correct data
     * type and form - default).
     *
     * @param telephoneNumber
     *         the phone number to be converted to Long, or set a default value (in the case of a
     *         non-entered phone number).
     *
     * @return a phone number in the Long data type may be the default (-1) or a specified phone
     * number in the form text box.
     */
    Long parseTelephoneNumber(String telephoneNumber);
}