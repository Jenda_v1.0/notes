package cz.uhk.umte.kruncja1.notes.team.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabaseImpl;

/**
 * Used for "data management". In the constructor it gets all the data (among other things) and as
 * the user scrolls the content, the individual items are redrawn quickly and only the data to be
 * displayed is transferred to the displayed parts - what is solved here -> what data are currently
 * displayed.
 */

public class TeamAdapter extends RecyclerView.Adapter<TeamViewHolder> {

    private final Map<String, Team> teams;

    private final OnItemTeamClick onItemTeamClick;

    private final TeamDatabase teamDatabase;

    TeamAdapter(Map<String, Team> teams, OnItemTeamClick onItemTeamClick) {
        this.teams = teams;
        this.onItemTeamClick = onItemTeamClick;

        teamDatabase = new TeamDatabaseImpl();
    }

    @NonNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.fragment_team_item, viewGroup, false);
        return new TeamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder teamViewHolder, final int i) {
        // Obtaining document Id in database:
        final List<String> keys = new ArrayList<>(teams.keySet());
        final String teamDocumentId = keys.get(i);

        final Team team = teams.get(teamDocumentId);

        final View.OnClickListener deleteTeamOnClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Removing an item from the map in memory:
                teams.remove(teamDocumentId);

                // Delete from database:
                teamDatabase.deleteByDocumentId(teamDocumentId, team.getBoardsUid(), v.getContext());

                // To reflect the changes in the recycler view:
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, teams.size());
            }
        };

        teamViewHolder.setTeamData(teamDocumentId, team, deleteTeamOnClickListener, onItemTeamClick);
    }

    @Override
    public int getItemCount() {
        return teams.size();
    }

    /**
     * Update displayed items in recycler view.
     */
    void updateItems() {
        // To reflect the changes in the recycler view:
        notifyDataSetChanged();
    }
}