package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Implementation of database operations with boards.
 */

public class BoardDatabaseImpl implements BoardDatabase {

    private static final TopicDatabase TOPIC_DATABASE = new TopicDatabaseImpl();

    private static final TeamDatabase TEAM_DATABASE = new TeamDatabaseImpl();

    @Override
    public void add(Board board, final Context context, OnSuccessListener<DocumentReference> successListener) {
        Log.w("ADD BOARD", "Add new board to DB: " + board);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.BOARD.getName())
                .add(board)
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ADD BOARD FAILURE", "Failed to create new board.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_add_board_failure));
                    }
                });
    }

    @Override
    public void update(String documentId, final Board board, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("UPDATE BOARD", "Update existing board in DB with id: '" + documentId + "', new board: " + board);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        final DocumentReference document = db.collection(DbCollection.BOARD.getName()).document(documentId);

        document.update("ownerUid", board.getOwnerUid());// not necessary
        document.update("name", board.getName());
        document.update("description", board.getDescription());
        // (createdAt does not change)
        document.update("lastEditAt", Timestamp.now())
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("UPDATE BOARD FAILURE", "Failed to update existing board: " + board);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_update_board_failure));
                    }
                });
    }

    @Override
    public void getByDocumentUid(final String uid, final Context context, OnSuccessListener<DocumentSnapshot> successListener) {
        Log.w("GET BOARD BY ITS D UID", "Get board by its document / record id: " + uid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.BOARD.getName())
                .document(uid)
                .get()
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("GET BOARD BY ITS D UID", "Failed to get board with board document id: " + uid);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_obtain_board_by_document_id_failed));
                    }
                });
    }

    @Override
    public void getByUserUid(String uid, OnCompleteListener<QuerySnapshot> completeListener) {
        Log.w("GET BOARDS BY USER UID", "Get boards created by user id (userUid): " + uid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.BOARD.getName())
                .whereEqualTo("ownerUid", uid)
                .get()
                .addOnCompleteListener(completeListener);
    }

    @Override
    public void deleteByDocumentId(final String boardUid, final Context context) {
        Log.w("DELETE BOARD BY DOC. ID", "Delete board with document id: " + boardUid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // Delete all topics that belong to the board with boardUid to be deleted:
        db.collection(DbCollection.TOPIC.getName())
                .whereEqualTo("boardUid", boardUid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.w("GET TOPICS BY BOARD UID", "All topics with board uid '" + boardUid + "' successfully found.");

                            for (final QueryDocumentSnapshot document : task.getResult()) {
                                Log.w("DELETE TOPIC", "Delete topic with uid: " + document.getId());
                                // Delete topic by its document id:
                                TOPIC_DATABASE.deleteByDocumentId(document.getId(), context);
                            }
                        } else {
                            Log.w("GET TOPICS BY BOARD UID", "Failed to get topics with board uid: " + boardUid);
                            ToastHelper.showToast(context, context.getString(R.string.db_impl_obtain_topics_in_delete_board_failed));
                        }
                    }
                });

        // Deleting board from all teams
        deleteBoardFromAllTeams(boardUid, context);

        // Delete board with boardUid as document ID in database:
        db.collection(DbCollection.BOARD.getName())
                .document(boardUid)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("DELETE BOARD BY DOC. ID", "Board successfully deleted.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_board_success));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DELETE BOARD BY DOC. ID", "An error occurred while trying to delete board with id: " + boardUid, e);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_board_failure));
                    }
                });
    }

    /**
     * Deleting boardUid board from all teams.
     *
     * @param boardUid
     *         document / record id of the board in the database to be deleted from all teams.
     * @param context
     *         is needed to display success / failure reports (Toast).
     */
    private static void deleteBoardFromAllTeams(final String boardUid, final Context context) {
        Log.w("DELETE BOARD FROM TEAMS", "Delete board with document id: '" + boardUid + "' from all teams.");

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // Retrieve all teams that have boardUid in the 'boardsUid' field, from each team will then be deleted the board with that id
        db.collection(DbCollection.TEAM.getName())
                .whereArrayContains("boardsUid", boardUid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                                final String teamDocumentId = documentSnapshot.getId();
                                Log.w("DELETE BOARD FROM TEAMS", "Delete board with document id: '" + boardUid + "' from team with document id '" + teamDocumentId + "'.");

                                final Team team = documentSnapshot.toObject(Team.class);
                                team.getBoardsUid().remove(boardUid);
                                TEAM_DATABASE.update(teamDocumentId, team, context, createUpdateTeamOnSuccessListener(teamDocumentId));
                            }
                        } else {
                            Log.w("DELETE BOARD FROM TEAMS", "Team data could not be obtained to delete board uid from team.");
                            ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_board_from_team_failure));
                        }
                    }
                });
    }

    /**
     * Create a listener to successfully delete a board with teamDocumentId from team, which is used
     * to log this information.
     *
     * @param teamDocumentId
     *         document / record id (team) from which the board was successfully deleted.
     *
     * @return listener described above.
     */
    private static OnSuccessListener<Void> createUpdateTeamOnSuccessListener(final String teamDocumentId) {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("DELETE BOARD FROM TEAMS", "Board successfully delete from the team with document id '" + teamDocumentId + "'.");
            }
        };
    }
}