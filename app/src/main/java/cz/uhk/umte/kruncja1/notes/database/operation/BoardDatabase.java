package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import cz.uhk.umte.kruncja1.notes.database.entity.Board;

/**
 * Database operations with boards.
 *
 * <i>The Firebase database can be empty; after the first entity is inserted, the necessary
 * structure is created.</i>
 */

public interface BoardDatabase {

    /**
     * Create a new board in the database (create a new record in the database).
     *
     * @param board
     *         new board to create (insert as a new record in the database).
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully create a DB record, which then displays all boards belonging
     *         to the signed in user.
     */
    void add(Board board, final Context context, OnSuccessListener<DocumentReference> successListener);

    /**
     * Updating an existing board in the database.
     *
     * @param documentId
     *         document id / existing board in the database to be updated.
     * @param board
     *         board with new data.
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener to successfully update a DB record, which then displays all boards belonging
     *         to the signed in user.
     */
    void update(String documentId, final Board board, final Context context, OnSuccessListener<Void> successListener);

    /**
     * Get board with document / record id (uid).
     *
     * @param uid
     *         the document ID (/ board) in the database to be retrieved
     * @param context
     *         is needed to display success / failure reports (Toast).
     * @param successListener
     *         listener saves a successfully loaded board in the phone memory.
     */
    void getByDocumentUid(String uid, final Context context, OnSuccessListener<DocumentSnapshot> successListener);

    /**
     * Obtain all boards belonging to the signed in user (user with document id in uid).
     *
     * @param uid
     *         the ID of the document in the database of the signed in user whose boards are to be
     *         loaded.
     * @param completeListener
     *         listener, which is used to store the boards into the application memory and to report
     *         info about success / failure.
     */
    void getByUserUid(String uid, OnCompleteListener<QuerySnapshot> completeListener);

    /**
     * Deleting the board and all data related to it from the database.
     *
     * @param boardUid
     *         document / record id in the database to be deleted.
     * @param context
     *         is needed to display success / failure reports (Toast).
     */
    void deleteByDocumentId(final String boardUid, final Context context);
}