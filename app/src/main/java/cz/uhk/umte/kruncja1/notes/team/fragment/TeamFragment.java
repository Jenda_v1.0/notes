package cz.uhk.umte.kruncja1.notes.team.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;

/**
 * This class is in charge (the quick rewriting of the data currently being displayed to the user),
 * which is set in the activity where the relevant data is to be displayed (in this case, teams).
 */

public class TeamFragment extends Fragment {

    private TeamAdapter teamAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Loading content to view from xml file:
        return inflater.inflate(R.layout.fragment_team_list, container, false);
    }

    /**
     * Set the necessary data and operations for the components that are currently displayed.
     *
     * @param teams
     *         all teams to display (the current boards will be redrawn in a certain order as user
     *         scroll)
     * @param onItemTeamClick
     *         some events on buttons
     */
    public void setDate(Map<String, Team> teams, OnItemTeamClick onItemTeamClick) {
        // The view will be created, onCreateView will be created before this setData method is called, so the necessary values ​​are created here:
        final RecyclerView recyclerView = getView().findViewById(R.id.teamRecyclerView);

        teamAdapter = new TeamAdapter(teams, onItemTeamClick);
        recyclerView.setAdapter(teamAdapter);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    /**
     * Updates items (teams) displayed in the fragment (for example, after leaving the team so that
     * the team is no longer displayed, etc.)
     */
    public void updateTeamFragment() {
        if (teamAdapter != null) {
            teamAdapter.updateItems();
        }
    }
}