package cz.uhk.umte.kruncja1.notes.note.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Note;
import cz.uhk.umte.kruncja1.notes.database.operation.NoteDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.NoteDatabaseImpl;

/**
 * Used for "data management". In the constructor it gets all the data (among other things) and as
 * the user scrolls the content, the individual items are redrawn quickly and only the data to be
 * displayed is transferred to the displayed parts - what is solved here -> what data are currently
 * displayed.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    private final Map<String, Note> notes;

    private final OnItemNoteClick onItemNoteClick;

    private final NoteDatabase noteDatabase;

    NoteAdapter(Map<String, Note> notes, OnItemNoteClick onItemNoteClick) {
        this.notes = notes;
        this.onItemNoteClick = onItemNoteClick;

        noteDatabase = new NoteDatabaseImpl();
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.fragment_note_item, viewGroup, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, final int i) {
        final List<String> keys = new ArrayList<>(notes.keySet());
        final String noteDocumentId = keys.get(i);

        final Note note = notes.get(noteDocumentId);

        final View.OnClickListener deleteNoteOnClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Removing an item from the map in memory:
                notes.remove(noteDocumentId);

                // Delete from database:
                noteDatabase.deleteByDocumentId(noteDocumentId, v.getContext());

                // To reflect the changes in the recycler view.
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, notes.size());
            }
        };

        noteViewHolder.setNoteData(noteDocumentId, note, deleteNoteOnClickListener, onItemNoteClick);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }
}