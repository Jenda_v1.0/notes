package cz.uhk.umte.kruncja1.notes.board;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.MainActivity;
import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.activity.ActionKind;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TeamDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.database.operation.TopicDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.TopicDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.team.TeamOverviewActivity;
import cz.uhk.umte.kruncja1.notes.texthelper.TextHelper;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;
import cz.uhk.umte.kruncja1.notes.topic.TopicActivity;
import cz.uhk.umte.kruncja1.notes.topic.fragment.OnItemTopicClick;
import cz.uhk.umte.kruncja1.notes.topic.fragment.TopicFragment;

/**
 * The main activity that displays all boards of signed in user. Next button to create a new board
 * and drop-down menu to log out, go to settings, etc.
 */

public class BoardActivity extends AppCompatActivity implements OnItemTopicClick {

    private EditText txtName, txtDescription;

    private BoardDatabase boardDatabase;
    private TeamDatabase teamDatabase;

    private TextValidation textValidation;

    private String documentId;
    /**
     * it is filled when the board is edited or displayed (its data).
     */
    private Board board;
    /**
     * All topics related to the selected board.
     */
    private Map<String, Topic> topics;

    private Team selectedTeam;
    private String teamDocumentId;

    private User signedInUser;
    private String userDocumentId;

    private Class<?> startActivityAfterEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Displaying activity content based on whether it is a creating new board or editing an existing board or show data of the selected board:
        final Intent intent = getIntent();
        final ActionKind actionkind = (ActionKind) intent.getSerializableExtra("actionKind");

        if (actionkind == ActionKind.CREATE) {
            setTitle(R.string.app_title_create_board);
            setContentView(R.layout.activity_create_board);

            // In the case of creating a board for the team, the following values ​​must be forwarded for subsequent handover to the activity
            signedInUser = intent.getExtras().getParcelable("signedInUser");
            userDocumentId = intent.getStringExtra("userDocumentId");

            // In the case of creating a board for a team, it is necessary to save the document ID of the board created in the team (for further handover and editing)
            selectedTeam = intent.getExtras().getParcelable("selectedTeam");
            teamDocumentId = intent.getStringExtra("teamDocumentId");

            txtName = findViewById(R.id.boardNameCreateInput);
            txtDescription = findViewById(R.id.boardDescriptionCreateInput);
        } else if (actionkind == ActionKind.EDIT) {
            setContentView(R.layout.activity_edit_board);

            // In the case of editing a board for the team, the following values ​​must be forwarded for subsequent handover to the activity
            signedInUser = intent.getExtras().getParcelable("signedInUser");
            userDocumentId = intent.getStringExtra("userDocumentId");

            // In the case of editing a board for a team, it is necessary to save the document ID of the board edited in the team (for further handover and editing)
            selectedTeam = intent.getExtras().getParcelable("selectedTeam");
            teamDocumentId = intent.getStringExtra("teamDocumentId");

            try {
                final String classname = intent.getStringExtra("startIntentAfterEdit");
                if (classname != null) {
                    startActivityAfterEdit = Class.forName(classname);
                }
            } catch (ClassNotFoundException e) {
                Log.w("CONVERTING CLASS", "There was an error trying to convert text (class name with packages) to a Java class.", e);
            }

            board = intent.getExtras().getParcelable("board");
            // ID of the selected board in the database:
            documentId = intent.getStringExtra("documentId");

            setTitle(getString(R.string.app_title_edit_text) + " '" + board.getName() + "'");

            txtName = findViewById(R.id.boardNameEditInput);
            txtDescription = findViewById(R.id.boardDescriptionEditInput);

            txtName.setText(board.getName());
            txtDescription.setText(board.getDescription());
        } else if (actionkind == ActionKind.SHOW) {// only "else" is enough
            setTitle(R.string.app_title_board);
            // This is just a display of the selected board data:
            setContentView(R.layout.activity_show_board);

            board = intent.getExtras().getParcelable("board");
            // ID of the selected board in the database:
            documentId = intent.getStringExtra("documentId");

            final TextView tvName = findViewById(R.id.asbBoardName);
            final TextView tvDescription = findViewById(R.id.asbBoardDescription);
            final TextView tvCreatedAt = findViewById(R.id.asbBoardCreatedAt);
            final TextView tvLastEditAt = findViewById(R.id.asbBoardLastEditAt);

            tvName.setText(board.getName());
            tvDescription.setText(board.getDescription());
            final String createdAtText = "Created at: " + TextHelper.getTextFromTimeStamp(board.getCreatedAt());
            tvCreatedAt.setText(createdAtText);
            final String lastEditAtText = "Last edit at: " + TextHelper.getTextFromTimeStamp(board.getLastEditAt());
            tvLastEditAt.setText(lastEditAtText);

            // Loading topics belonging to the selected board to display:
            topics = new LinkedHashMap<>();

            final TopicDatabase topicDatabase = new TopicDatabaseImpl();
            topicDatabase.getByBoardUid(documentId, new GetTopicsBelongingToSelectedBoardUidOnCompleteListener());
        }

        textValidation = new TextValidationImpl();

        boardDatabase = new BoardDatabaseImpl();
        teamDatabase = new TeamDatabaseImpl();
    }

    /**
     * It is called when you click the button to create a new board in activity_create_board.xml.
     *
     * <i>The data entered in the text boxes is tested and if the data is valid, a new board is
     * created and inserted into the database.</i>
     *
     * @param view
     */
    public void onBtnCreateBoardClick(View view) {
        final boolean validatedBoardName = validateBoardName();
        if (!validatedBoardName) {
            Log.w("CREATE NEW BOARD", "Name is not valid.");
            return;
        }

        Log.w("CREATE NEW BOARD", "Name and description is valid, continuing to create new board and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(txtName);
        final String description = textValidation.getTextFromEditText(txtDescription);

        // Create new Board (add new record to DB):
        final Board board = new Board(getUserUidFromIntent(), name, description);
        boardDatabase.add(board, this, createAddBoardSuccessListener());
    }

    /**
     * It is called when you click the button to edit an existing board in activity_edit_board.xml.
     *
     * <i>If the board name is valid, it is updated in the database.</i>
     *
     * @param view
     */
    public void onBtnEditBoardClick(View view) {
        final boolean validatedBoardName = validateBoardName();
        if (!validatedBoardName) {
            Log.w("UPDATE BOARD", "Name is not valid.");
            return;
        }

        Log.w("UPDATE BOARD", "Name and description is valid, continuing to update existing board and upload to DB.");

        // Texts from text fields:
        final String name = textValidation.getTextFromEditText(txtName);
        final String description = textValidation.getTextFromEditText(txtDescription);

        board.setName(name);
        board.setDescription(description);

        boardDatabase.update(documentId, board, this, createEditBoardSuccessListener());
    }

    /**
     * Validation of the board name entered to txtName.
     *
     * @return true if the name is valid, otherwise false.
     */
    private boolean validateBoardName() {
        // Error texts for invalidate values:
        final String emptyName = getString(R.string.act_board_empty_name);
        final String invalidMinLengthName = getString(R.string.act_board_invalid_min_length_name);
        final String invalidMaxLengthName = getString(R.string.act_board_invalid_max_length_name);

        final boolean validatedName = textValidation.validateBasicTextField(txtName, emptyName, invalidMinLengthName, invalidMaxLengthName);

        if (!validatedName) {
            Log.w("BOARD NAME LENGTH", "Invalid board name length in text field.");
            return false;
        }

        Log.w("BOARD NAME LENGTH", "Valid board name length in text field.");
        return true;
    }

    private String getUserUidFromIntent() {
        final Intent intent = getIntent();
        return intent.getStringExtra("userUid");
    }

    /**
     * If the board is successfully created, the default activity will start with a all boards
     * overview.
     *
     * @return a listener performing an action if the board is successfully created (runs an
     * activity with all boards overview).
     */
    private OnSuccessListener<DocumentReference> createAddBoardSuccessListener() {
        return new OnSuccessListener<DocumentReference>() {

            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.w("ADD BOARD SUCCESS", "Board successfully created / added to DB.");
                ToastHelper.showToast(BoardActivity.this, getString(R.string.act_board_add_board_success));

                if (selectedTeam != null && teamDocumentId != null) {
                    // This is about creating a new board for an existing team

                    // Adding the document ID of the just created board in the database to the list on the team
                    selectedTeam.getBoardsUid().add(documentReference.getId());
                    // Updating the team in the database (adding a new board to the team)
                    teamDatabase.update(teamDocumentId, selectedTeam, BoardActivity.this, createUpdateTeamSuccessListener());

                    startTeamOverviewActivity();
                } else {
                    // Here the signed in user can create his own board (only for himself, not for team)
                    startActivity(new Intent(BoardActivity.this, MainActivity.class));
                    finish();
                }
            }
        };
    }

    private void startTeamOverviewActivity() {
        final Intent teamOverviewIntent = new Intent(BoardActivity.this, TeamOverviewActivity.class);

        teamOverviewIntent.putExtra("selectedTeam", selectedTeam);
        teamOverviewIntent.putExtra("teamDocumentId", teamDocumentId);
        teamOverviewIntent.putExtra("userDocumentId", userDocumentId);
        teamOverviewIntent.putExtra("signedInUser", signedInUser);

        startActivity(teamOverviewIntent);
        finish();
    }

    /**
     * Creating a listener that writes to the log information about a successful team update in the
     * database.
     *
     * @return listener described above.
     */
    private OnSuccessListener<Void> createUpdateTeamSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("TEAM UPDATED", "Team successfully updated in database.");
            }
        };
    }

    /**
     * If the board is successfully updated, the default ({@link MainActivity}) activity will start
     * with a all boards overview.
     *
     * @return a listener performing an action if the board is successfully updated (runs an
     * activity with all boards overview).
     */
    private OnSuccessListener<Void> createEditBoardSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE BOARD SUCCESS", "Board successfully updated in DB.");
                ToastHelper.showToast(BoardActivity.this, getString(R.string.db_impl_update_board_success));

                // In the case that (among other things) the following objects in condition are available, TeamOverviewActivity should be launched because it is a team-modified board
                if (startActivityAfterEdit != null && selectedTeam != null && teamDocumentId != null) {
                    startTeamOverviewActivity();
                } else {
                    startActivity(new Intent(BoardActivity.this, MainActivity.class));
                    finish();
                }
            }
        };
    }

    /**
     * The method will only be available if the "activity_show_board.xml" context is displayed
     * (representation of one selected board [ActionKind.SHOW]).
     *
     * <i>Deletes the marked board from the database and launches the {@link MainActivity} with an
     * overview of all boards.</i>
     *
     * @param view
     */
    public void asbBtnDeleteBoardClick(View view) {
        Log.w("DELETE SELECTED BOARD", "Delete selected board from database.");

        // Delete from database:
        boardDatabase.deleteByDocumentId(documentId, this);

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    /**
     * The method will only be available if the "activity_show_board.xml" context is displayed
     * (representation of one selected board [ActionKind.SHOW]).
     *
     * <i>Runs the activity for editing the marked board and passes the necessary data (board,
     * document id in the database).</i>
     *
     * @param view
     */
    public void asbBtnEditBoardClick(View view) {
        Log.w("EDIT SELECTED BOARD", "Start BoardActivity for edit selected board.");

        final Intent boardIntent = new Intent(this, BoardActivity.class);

        boardIntent.putExtra("actionKind", ActionKind.EDIT);
        boardIntent.putExtra("board", board);
        boardIntent.putExtra("documentId", documentId);

        startActivity(boardIntent);
        finish();
    }

    /**
     * Start activity to create a new topic for the currently displayed / selected board.
     *
     * @param view
     */
    public void asbBtnCreateTopicClick(View view) {
        Log.w("CREATE NEW TOPIC", "Start TopicActivity for create a new topic.");

        final Intent topicIntent = new Intent(this, TopicActivity.class);

        topicIntent.putExtra("actionKind", ActionKind.CREATE);
        topicIntent.putExtra("boardDocumentId", documentId);
        // There is need to pass the selected board to view it back:
        topicIntent.putExtra("selectedBoard", board);

        startActivity(topicIntent);
        finish();
    }

    @Override
    public void onEditTopicClick(String topicDocumentId, Topic topic) {
        Log.w("EDIT SELECTED TOPIC", "Start TopicActivity for edit selected topic.");

        final Intent topicIntent = new Intent(this, TopicActivity.class);

        topicIntent.putExtra("actionKind", ActionKind.EDIT);
        topicIntent.putExtra("selectedBoard", board);
        topicIntent.putExtra("boardDocumentId", documentId);
        topicIntent.putExtra("selectedTopic", topic);
        topicIntent.putExtra("topicDocumentId", topicDocumentId);

        startActivity(topicIntent);
        finish();
    }

    @Override
    public void onTopicClick(String topicDocumentId, Topic topic) {
        Log.w("OPEN TOPIC OVERVIEW", "Start TopicActivity for show selected topic overview.");

        final Intent topicIntent = new Intent(this, TopicActivity.class);

        topicIntent.putExtra("actionKind", ActionKind.SHOW);
        topicIntent.putExtra("selectedBoard", board);
        topicIntent.putExtra("selectedTopic", topic);
        topicIntent.putExtra("boardDocumentId", documentId);
        topicIntent.putExtra("topicDocumentId", topicDocumentId);

        startActivity(topicIntent);
        finish();
    }

    /**
     * Listener, which is used to load all topics of the related to selected board of signed in
     * user. Loaded topics will be stored in the topics collection. Next, the relevant topics will
     * appear in the activity content, etc.
     */
    private class GetTopicsBelongingToSelectedBoardUidOnCompleteListener implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.w("GET TOPIC BY BOARD UID", "Topic data was successfully obtained: " + document.getData());

                    final Topic topicFromMap = Topic.getTopicFromMap(document.getData());
                    topics.put(document.getId(), topicFromMap);
                    Log.w("GET TOPIC BY BOARD UID", "Obtained topic successfully created: " + topicFromMap);
                }

                Log.w("GET TOPICS BY BOARD UID", "All successfully obtained topics: " + topics);
                // "Create" a fragment in which individual topics will show:
                final TopicFragment topicFragment = (TopicFragment) getSupportFragmentManager().findFragmentById(R.id.topicFragment);
                if (topicFragment != null) {
                    topicFragment.setDate(topics, BoardActivity.this);
                }
            } else {
                Log.w("GET TOPICS BY BOARD UID", "Topics data could not be obtained.");
                ToastHelper.showToast(BoardActivity.this, getString(R.string.act_board_topics_data_not_obtained));
            }
        }
    }
}