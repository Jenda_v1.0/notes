package cz.uhk.umte.kruncja1.notes.team.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;

/**
 * This is used to set values ​​to one specific item representing the team in the displayed list.
 *
 * <i>Through the appropriate methods, the particular team data that is displayed is set to the
 * component to be rendered.</i>
 */

class TeamViewHolder extends RecyclerView.ViewHolder {

    private TextView tvTeamName, tvTeamDescription;

    private Button btnLeaveTeam, btnEditTeam, btnDeleteTeam;

    TeamViewHolder(@NonNull View itemView) {
        super(itemView);

        tvTeamName = itemView.findViewById(R.id.itemTeamName);
        tvTeamDescription = itemView.findViewById(R.id.itemTeamDescription);

        btnLeaveTeam = itemView.findViewById(R.id.btnLeaveTeam);
        btnEditTeam = itemView.findViewById(R.id.btnEditTeam);
        btnDeleteTeam = itemView.findViewById(R.id.btnDeleteTeam);
    }

    void setTeamData(final String teamDocumentId, final Team team, View.OnClickListener deleteTeamOnClickListener, final OnItemTeamClick onItemTeamClick) {
        tvTeamName.setText(team.getName());
        tvTeamDescription.setText(team.getDescription());

        btnDeleteTeam.setOnClickListener(deleteTeamOnClickListener);

        btnLeaveTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTeamClick.onLeaveTeamClick(teamDocumentId, team);
            }
        });

        btnEditTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTeamClick.onEditTeamClick(teamDocumentId, team);
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTeamClick.onTeamClick(teamDocumentId, team);
            }
        });
    }
}