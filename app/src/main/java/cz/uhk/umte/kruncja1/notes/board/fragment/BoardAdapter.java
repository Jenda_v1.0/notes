package cz.uhk.umte.kruncja1.notes.board.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Board;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.BoardDatabaseImpl;

/**
 * Used for "data management". In the constructor it gets all the data (among other things) and as
 * the user scrolls the content, the individual items are redrawn quickly and only the data to be
 * displayed is transferred to the displayed parts - what is solved here -> what data are currently
 * displayed.
 */

public class BoardAdapter extends RecyclerView.Adapter<BoardViewHolder> {

    private final Map<String, Board> boards;

    private final OnItemBoardClick onItemBoardClick;

    private final BoardDatabase boardDatabase;

    BoardAdapter(Map<String, Board> boards, OnItemBoardClick onItemBoardClick) {
        this.boards = boards;
        this.onItemBoardClick = onItemBoardClick;

        boardDatabase = new BoardDatabaseImpl();
    }

    @NonNull
    @Override
    public BoardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.fragment_board_item, viewGroup, false);
        return new BoardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BoardViewHolder boardViewHolder, final int i) {
        // Obtaining document Id in database:
        final List<String> keys = new ArrayList<>(boards.keySet());
        final String boardDocumentId = keys.get(i);

        final Board board = boards.get(boardDocumentId);

        final View.OnClickListener deleteBoardOnClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Removing an item from the map in memory:
                boards.remove(boardDocumentId);

                // Delete from database:
                boardDatabase.deleteByDocumentId(boardDocumentId, v.getContext());

                // To reflect the changes in the recycler view.
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, boards.size());
            }
        };

        boardViewHolder.setBoardData(boardDocumentId, board, deleteBoardOnClickListener, onItemBoardClick);
    }

    @Override
    public int getItemCount() {
        return boards.size();
    }

    /**
     * Update displayed items in recycler view.
     */
    void updateItems() {
        // To reflect the changes in the recycler view:
        notifyDataSetChanged();
    }
}