package cz.uhk.umte.kruncja1.notes.database.operation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Team;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * Implementation of database operations with teams.
 */

public class TeamDatabaseImpl implements TeamDatabase {

    private static final BoardDatabase BOARD_DATABASE = new BoardDatabaseImpl();

    @Override
    public void getAllUsersInCollection(OnCompleteListener<QuerySnapshot> completeListener) {
        Log.w("GET ALL USERS", "Get all users in collection.");

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.USER.getName())
                .get()
                .addOnCompleteListener(completeListener);
    }

    @Override
    public void add(Team team, final Context context, OnSuccessListener<DocumentReference> successListener) {
        Log.w("ADD TEAM", "Add new team to DB: " + team);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.TEAM.getName())
                .add(team)
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ADD TEAM FAILURE", "Failed to create new team.");
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_add_team_failure));
                    }
                });
    }

    @Override
    public void update(String documentId, final Team team, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("UPDATE TEAM", "Update existing team in DB with id: '" + documentId + "', new team: " + team);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        final DocumentReference document = db.collection(DbCollection.TEAM.getName()).document(documentId);

        document.update("usersUid", team.getUsersUid());
        document.update("boardsUid", team.getBoardsUid());
        document.update("name", team.getName());
        document.update("description", team.getDescription());
        // (createdAt does not change)
        document.update("lastEditAt", Timestamp.now())
                .addOnSuccessListener(successListener)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("UPDATE TEAM FAILURE", "Failed to update existing team: " + team, e);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_update_team_failure));
                    }
                });
    }

    @Override
    public void getBySignedUserDocumentUid(String userDocumentUid, OnCompleteListener<QuerySnapshot> completeListener) {
        Log.w("GET TEAMS BY USER D UID", "Get teams related to user document uid: " + userDocumentUid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.TEAM.getName())
                .whereArrayContains("usersUid", userDocumentUid)
                .get()
                .addOnCompleteListener(completeListener);
    }

    @Override
    public void deleteByDocumentId(final String teamUid, List<String> boardsUid, final Context context) {
        Log.w("DELETE TEAM BY DOC. ID", "Delete team with its document id: " + teamUid + ", without success listener.");

        deleteTeamByDocumentId(teamUid, boardsUid, context, null);
    }

    @Override
    public void leave(String teamDocumentId, String userDocumentId, Team team, Context context, OnSuccessListener<Void> successListener) {
        Log.w("REMOVE USER FROM TEAM", "Remove a user with document id '" + userDocumentId + "' from the team " + team);

        team.getUsersUid().remove(userDocumentId);

        if (team.getUsersUid().isEmpty()) {
            // Delete team that has no members
            // (There is no need to show Toast with the team deletion result, but it means unnecessary conditions or extra listeners)
            Log.w("DELETE TEAM BY DOC. ID", "Delete team with its document id: " + teamDocumentId + ", with success listener");
            deleteTeamByDocumentId(teamDocumentId, team.getBoardsUid(), context, successListener);
            return;
        }

        // Update team (with signed in user / member removed) in database
        // (there is only need to update the collection with users - usersUid)
        update(teamDocumentId, team, context, successListener);
    }

    @Override
    public void deleteUserFromAllTeams(final String userDocumentId, final Context context) {
        Log.w("DELETE USER FROM TEAMS", "Delete user with his document / record id '" + userDocumentId + "' from all teams as a member.");

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        db.collection(DbCollection.TEAM.getName())
                .whereArrayContains("usersUid", userDocumentId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.w("DELETE USER FROM TEAMS", "All teams where user with document id '" + userDocumentId + "' is a member were successfully found.");

                            for (final QueryDocumentSnapshot document : task.getResult()) {
                                final Team teamFromMap = Team.getTeamFromMap(document.getData());
                                Log.w("DELETE USER FROM TEAMS", "Preparing for remove user with document id '" + userDocumentId + "' from the team: " + teamFromMap);

                                final OnSuccessListener<Void> removeUserFromTeamOnSuccessListener = new OnSuccessListener<Void>() {

                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.w("DELETE USER FROM TEAMS", "User with id '" + userDocumentId + "' was successfully removed from the team '" + teamFromMap + "' as its member.");
                                    }
                                };

                                leave(document.getId(), userDocumentId, teamFromMap, context, removeUserFromTeamOnSuccessListener);
                            }
                        } else {
                            Log.w("DELETE USER FROM TEAMS", "Failed to load all teams where user with his document id '" + userDocumentId + "' is a member of.");
                            ToastHelper.showToast(context, context.getString(R.string.db_impl_failed_to_load_all_teams_where_user_is_member));
                        }
                    }
                });
    }

    private static void deleteTeamByDocumentId(final String teamUid, List<String> boardsUid, final Context context, OnSuccessListener<Void> successListener) {
        Log.w("DELETE TEAM BY DOC. ID", "Delete team with its document id: " + teamUid);

        final FirebaseFirestore db = DbFireBaseFireStore.getDbConnection();

        // Every board has its founder (user), so it is not absolutely necessary to delete all boards in the team, but all changes in the team would see only its owner, so everything is deleted within the team
        for (String boardUid : boardsUid) {
            BOARD_DATABASE.deleteByDocumentId(boardUid, context);
        }

        final Task<Void> deleteTask = db.collection(DbCollection.TEAM.getName())
                .document(teamUid)
                .delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DELETE TEAM BY DOC. ID", "An error occurred while trying to delete team with id: " + teamUid, e);
                        ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_team_failure));
                    }
                });

        if (successListener != null) {
            deleteTask.addOnSuccessListener(successListener);
        } else {
            deleteTask.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("DELETE TEAM BY DOC. ID", "Team successfully deleted.");
                    ToastHelper.showToast(context, context.getString(R.string.db_impl_delete_team_success));
                }
            });
        }
    }
}