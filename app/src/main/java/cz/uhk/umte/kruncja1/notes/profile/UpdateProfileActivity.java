package cz.uhk.umte.kruncja1.notes.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.User;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabase;
import cz.uhk.umte.kruncja1.notes.database.operation.UserDatabaseImpl;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidation;
import cz.uhk.umte.kruncja1.notes.edittextvalidation.TextValidationImpl;
import cz.uhk.umte.kruncja1.notes.login.LoginActivity;
import cz.uhk.umte.kruncja1.notes.register.validation.FormDataValidation;
import cz.uhk.umte.kruncja1.notes.register.validation.FormDataValidationImpl;
import cz.uhk.umte.kruncja1.notes.texthelper.TextHelper;
import cz.uhk.umte.kruncja1.notes.toast.ToastHelper;

/**
 * It is used for the form for changing data in the signed in user's profile.
 *
 * <i>If the email address changes, the link to cancel the change and return the original email
 * address is sent to the original email address. A verification link is sent to the newly entered
 * email address to verification email address and sign out from the application, only after
 * verifying the new e-mail address and then sign in, it is possible to view the data.</i>
 *
 * <i>If the email address does not change, the changed data is immediately applied to the
 * database.</i>
 */

public class UpdateProfileActivity extends AppCompatActivity {

    private EditText etEmail, etUsername, etFirstName, etLastName, etTelephoneNumber, etConfirmPassword;

    /**
     * Signed in user.
     */
    private User signedInUser;
    /**
     * The record ID in the database that contains the signed in user information. This id is needed
     * to update a user document / record in the database.
     */
    private String userDocumentId;

    private TextValidation textValidation;
    private FormDataValidation formDataValidation;

    private FirebaseAuth firebaseAuth;

    private UserDatabase userDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_title_edit_profile);
        setContentView(R.layout.activity_update_profile);

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        signedInUser = getIntent().getExtras().getParcelable("signedInUser");
        userDocumentId = getIntent().getStringExtra("userDocumentId");

        etEmail = findViewById(R.id.editProfileEmailInput);
        etUsername = findViewById(R.id.editProfileUsernameInput);
        etFirstName = findViewById(R.id.editProfileFirstNameInput);
        etLastName = findViewById(R.id.editProfileLastNameInput);
        etTelephoneNumber = findViewById(R.id.editProfileTelephoneNumberInput);
        etConfirmPassword = findViewById(R.id.editProfileConfirmPasswordInput);

        etEmail.setText(signedInUser.getEmail());
        etUsername.setText(signedInUser.getUsername());
        etFirstName.setText(signedInUser.getFirstName());
        etLastName.setText(signedInUser.getLastName());

        if (signedInUser.getTelephoneNumber() != null && signedInUser.getTelephoneNumber() > -1) {
            final String formattedTelephoneNumber = TextHelper.formatTelephoneNumber(signedInUser.getTelephoneNumber().toString(), TextHelper.REG_EX_REPLACEMENT_TELEPHONE_NUMBER_WITH_SPACE);
            etTelephoneNumber.setText(formattedTelephoneNumber);
        }

        textValidation = new TextValidationImpl();
        formDataValidation = new FormDataValidationImpl();

        userDatabase = new UserDatabaseImpl();
    }

    /**
     * Validating entered values ​​in text boxes, and possibly updating profile values ​​in the
     * database and running activity to display the profile.
     *
     * @param view
     */
    public void onBtnEditProfileClick(View view) {
        // Error texts for empty fields:
        final String emptyEmail = getString(R.string.act_update_profile_empty_email);
        final String emptyUsername = getString(R.string.act_update_profile_empty_username);
        final String emptyFirstName = getString(R.string.act_update_profile_empty_first_name);
        final String emptyLastName = getString(R.string.act_update_profile_empty_last_name);
        final String emptyPassword = getString(R.string.act_update_profile_empty_password);

        // Error texts for invalidate values:
        final String invalidMinLengthEmail = getString(R.string.act_update_profile_invalid_min_length_email);
        final String invalidMinLengthUsername = getString(R.string.act_update_profile_invalid_min_length_username);
        final String invalidMinLengthFirstName = getString(R.string.act_update_profile_invalid_min_length_first_name);
        final String invalidMinLengthLastName = getString(R.string.act_update_profile_invalid_min_length_last_name);
        final String invalidMinLengthPassword = getString(R.string.act_update_profile_invalid_min_length_password);

        final String invalidMaxLengthEmail = getString(R.string.act_update_profile_invalid_max_length_email);
        final String invalidMaxLengthUsername = getString(R.string.act_update_profile_invalid_max_length_username);
        final String invalidMaxLengthFirstName = getString(R.string.act_update_profile_invalid_max_length_first_name);
        final String invalidMaxLengthLastName = getString(R.string.act_update_profile_invalid_max_length_last_name);
        final String invalidMaxLengthPassword = getString(R.string.act_update_profile_invalid_max_length_password);

        // Validation results (telephone number is not necessary):
        final boolean validatedEmail = textValidation.validateBasicTextField(etEmail, emptyEmail, invalidMinLengthEmail, invalidMaxLengthEmail);
        final boolean validatedUsername = textValidation.validateBasicTextField(etUsername, emptyUsername, invalidMinLengthUsername, invalidMaxLengthUsername);
        final boolean validatedFirstName = textValidation.validateBasicTextField(etFirstName, emptyFirstName, invalidMinLengthFirstName, invalidMaxLengthFirstName);
        final boolean validatedLastName = textValidation.validateBasicTextField(etLastName, emptyLastName, invalidMinLengthLastName, invalidMaxLengthLastName);
        final boolean validatedPassword = textValidation.validateBasicTextField(etConfirmPassword, emptyPassword, invalidMinLengthPassword, invalidMaxLengthPassword);

        if (!validatedEmail || !validatedUsername || !validatedFirstName || !validatedLastName || !validatedPassword) {
            Log.w("UPDATE PROF DATA LENGTH", "Invalid texts length in text fields.");
            return;
        }

        Log.w("UPDATE PROFILE", "Data in text fields are valid, trying to register new user.");

        final String invalidEmail = getString(R.string.act_update_profile_invalid_regex_email);
        final String invalidUsername = getString(R.string.act_update_profile_invalid_regex_username);
        final String invalidFirstName = getString(R.string.act_update_profile_invalid_regex_first_name);
        final String invalidLastName = getString(R.string.act_update_profile_invalid_regex_last_name);
        final String invalidTelephoneNumber = getString(R.string.act_update_profile_invalid_regex_telephone_number);

        final boolean validatedRegExEmail = formDataValidation.validateEmail(etEmail, invalidEmail);
        final boolean validatedRegExUsername = formDataValidation.validateUsername(etUsername, invalidUsername);
        final boolean validatedRegExFirstName = formDataValidation.validateFirstName(etFirstName, invalidFirstName);
        final boolean validatedRegExLastName = formDataValidation.validateLastName(etLastName, invalidLastName);
        final boolean validatedRegExTelephoneNumber = formDataValidation.validateTelephoneNumber(etTelephoneNumber, invalidTelephoneNumber);

        if (!validatedRegExEmail || !validatedRegExUsername || !validatedRegExFirstName || !validatedRegExLastName || !validatedRegExTelephoneNumber) {
            Log.w("UPDATE PROF TEXTS REGEX", "Invalid data in text fields.");
            return;
        }

        // At this time, the data in the form is valid:
        Log.w("UPDATE PROFILE", "The data in the text fields are valid, followed by a change in the data in the database.");

        // Texts form text fields:
        final String email = textValidation.getTextFromEditText(etEmail);
        final String username = textValidation.getTextFromEditText(etUsername);
        final String firstName = textValidation.getTextFromEditText(etFirstName);
        final String lastName = textValidation.getTextFromEditText(etLastName);
        final String telephoneNumber = textValidation.getTextFromEditText(etTelephoneNumber);
        final String password = textValidation.getTextFromEditText(etConfirmPassword);


        // To set data from form to object (for update):
        signedInUser.setUsername(username);
        signedInUser.setFirstName(firstName);
        signedInUser.setLastName(lastName);
        signedInUser.setEmail(email);
        signedInUser.setTelephoneNumber(userDatabase.parseTelephoneNumber(telephoneNumber));

        // FIXME: User must enter a password in the form in any case, but it will only be used if it is a change of email address (to resolve otherwise or to display a text field only when changing email)

        // If emails are different, there is need to update them "special":
        if (firebaseAuth.getCurrentUser() != null && firebaseAuth.getCurrentUser().getEmail() != null && !email.equals(firebaseAuth.getCurrentUser().getEmail())) {
            // Sign in again is needed to change email:
            firebaseAuth.signInWithEmailAndPassword(firebaseAuth.getCurrentUser().getEmail(), password)
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            Log.w("SIGN IN USER AGAIN", "User successfully signed in again.");

                            firebaseAuth.getCurrentUser()
                                    .updateEmail(email)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.w("UPDATE EMAIL", "Email was successfully updated.");

                                            // Updating database data:
                                            userDatabase.update(userDocumentId, signedInUser, UpdateProfileActivity.this);

                                            // Email address has been successfully changed here, verification email will be sent:
                                            firebaseAuth.getCurrentUser().sendEmailVerification()
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Log.w("SEND VERIFICATION EMAIL", "Verification email was successfully sent.");

                                                            Log.w("LOG OUT", "Sign out user.");
                                                            firebaseAuth.signOut();

                                                            if (firebaseAuth.getCurrentUser() == null) {
                                                                Log.w("LOG OUT", "User was successfully signed out, redirect to LoginActivity.");

                                                                ToastHelper.showToast(UpdateProfileActivity.this, getString(R.string.act_update_profile_verify_email_and_sign_in));

                                                                startActivity(new Intent(UpdateProfileActivity.this, LoginActivity.class));
                                                                finish();
                                                            }

                                                        }
                                                    })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.w("SEND VERIFICATION EMAIL", "There was an error trying to send a verification email.", e);
                                                            ToastHelper.showToast(UpdateProfileActivity.this, getString(R.string.act_update_profile_verification_email_send_failure));
                                                        }
                                                    });
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w("UPDATE EMAIL", "An error occurred while trying to change the email address.", e);
                                            ToastHelper.showToast(UpdateProfileActivity.this, getString(R.string.act_update_profile_update_email_address_failure));
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("SIGN IN USER AGAIN", "There was an error trying to sign in to change the email address.", e);
                            ToastHelper.showToast(UpdateProfileActivity.this, getString(R.string.act_update_profile_sign_in_to_update_email_failure));
                        }
                    });
        } else {
            // Updating database data (not an email address change):
            userDatabase.update(userDocumentId, signedInUser, UpdateProfileActivity.this, createUpdateUserSuccessListener());
        }
    }


    /**
     * If the user is successfully updated, the {@link ProfileActivity} will start and display
     * edited profile data of the signed in user
     *
     * @return a listener performing an action if the user is successfully updated (runs an activity
     * to show changes).
     */
    private OnSuccessListener<Void> createUpdateUserSuccessListener() {
        return new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.w("UPDATE USER SUCCESS", "User successfully updated in DB.");

                ToastHelper.showToast(UpdateProfileActivity.this, getString(R.string.act_update_profile_update_user_success));

                final Intent profileIntent = new Intent(UpdateProfileActivity.this, ProfileActivity.class);

                profileIntent.putExtra("signedInUser", signedInUser);
                profileIntent.putExtra("userDocumentId", userDocumentId);

                startActivity(profileIntent);
                finish();
            }
        };
    }
}