package cz.uhk.umte.kruncja1.notes.topic.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Map;

import cz.uhk.umte.kruncja1.notes.R;
import cz.uhk.umte.kruncja1.notes.database.entity.Topic;

/**
 * This class is in charge (the quick rewriting of the data currently being displayed to the user),
 * which is set in the activity where the relevant data is to be displayed (in this case, topics).
 */

public class TopicFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Loading content to view from xml file:
        return inflater.inflate(R.layout.fragment_topic_list, container, false);
    }

    /**
     * Set the necessary data and operations for the components that are currently displayed.
     *
     * @param topics
     *         all topics to display (the current boards will be redrawn in a certain order as user
     *         scroll)
     * @param onItemTopicClick
     *         some events on buttons
     */
    public void setDate(Map<String, Topic> topics, OnItemTopicClick onItemTopicClick) {
        // The view will be created, onCreateView will be created before this setData method is called, so the necessary values ​​are created here:
        final RecyclerView recyclerView = getView().findViewById(R.id.topicRecyclerView);

        final RecyclerView.Adapter adapter = new TopicAdapter(topics, onItemTopicClick);
        recyclerView.setAdapter(adapter);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }
}