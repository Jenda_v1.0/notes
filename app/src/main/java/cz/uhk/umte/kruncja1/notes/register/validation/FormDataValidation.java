package cz.uhk.umte.kruncja1.notes.register.validation;

import android.content.Context;
import android.widget.EditText;

/**
 * Validating values ​​in text fields according to regular expressions, lengths, etc.
 *
 * <i>Methods return true if the text is valid, otherwise it returns false, and possibly displays
 * an error message.</i>
 */

public interface FormDataValidation {

    boolean validateEmail(EditText editText, String errorText);

    boolean validateUsername(EditText editText, String errorText);

    boolean validateFirstName(EditText editText, String errorText);

    boolean validateLastName(EditText editText, String errorText);

    boolean validateTelephoneNumber(EditText editText, String errorText);

    boolean validatePassword(String password1, String password2, String errorText, Context context);
}