package cz.uhk.umte.kruncja1.notes.database.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The team contains IDs of users' and boards. Each team has at least one user and can have any
 * number of boards in the team.
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class Team extends AbstractEntity {

    /**
     * UID of the user document in the users collection.
     *
     * <i>If the UID of the users stored in Authentication were entered, it would not be possible
     * to obtain it without the Firebase Admin SDK (this is a complex and lengthy
     * configuration).</i>
     */
    private List<String> usersUid;

    /**
     * UID of the board document in the boards collection.
     */
    private List<String> boardsUid;

    public Team(List<String> usersUid, List<String> boardsUid, String name, String description) {
        super(name, description);
        this.usersUid = usersUid;
        this.boardsUid = boardsUid;
    }

    private Team(List<String> usersUid, List<String> boardsUid, String name, String description, Timestamp createdAt, Timestamp lastEditAt) {
        super(name, description, createdAt, lastEditAt);
        this.usersUid = usersUid;
        this.boardsUid = boardsUid;
    }

    /**
     * Creates the {@link Team} object from the data (attribute values) that are on the map.
     *
     * @param data
     *         attributes values.
     *
     * @return cz.uhk.umte.kruncja1.notes.database.entity.Team instance containing values from the
     * data map.
     */
    public static Team getTeamFromMap(Map<String, Object> data) {
        final List<String> usersUid = (List<String>) data.get("usersUid");
        final List<String> boardsUid = (List<String>) data.get("boardsUid");
        final String name = (String) data.get("name");
        final String description = (String) data.get("description");
        final Timestamp createdAt = (Timestamp) data.get("createdAt");
        final Timestamp lastEditAt = (Timestamp) data.get("lastEditAt");

        return new Team(usersUid, boardsUid, name, description, createdAt, lastEditAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // This is needed (done) when passing an instance of this class to Intent, so you need to "write" all values ​​(serialization replacement because TimeStamp is not serializable):
        dest.writeList(usersUid);
        dest.writeList(boardsUid);

        dest.writeString(name);
        dest.writeString(description);

        dest.writeInt(createdAt.getNanoseconds());
        dest.writeLong(createdAt.getSeconds());

        dest.writeInt(lastEditAt.getNanoseconds());
        dest.writeLong(lastEditAt.getSeconds());
    }


    public static final Parcelable.Creator<Team> CREATOR = new Parcelable.Creator<Team>() {
        public Team createFromParcel(Parcel in) {
            return new Team(in);
        }

        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    /**
     * The constructor is called when data is obtained from Intent and specific instances of this
     * class is created with the relevant data.
     *
     * @param in
     */
    private Team(Parcel in) {
        usersUid = new ArrayList<>();
        in.readList(usersUid, getClass().getClassLoader());

        boardsUid = new ArrayList<>();
        in.readList(boardsUid, getClass().getClassLoader());

        name = in.readString();
        description = in.readString();

        final int iCreatedAt = in.readInt();
        final long lCreatedAt = in.readLong();
        createdAt = new Timestamp(lCreatedAt, iCreatedAt);

        final int iLastEditAt = in.readInt();
        final long lLastEditAt = in.readLong();
        lastEditAt = new Timestamp(lLastEditAt, iLastEditAt);
    }
}