package cz.uhk.umte.kruncja1.notes.database.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Represents a board that includes topics and topics that contain notes.
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor // This constructor is needed for FireStore
public class Board extends AbstractEntity {

    /**
     * Document ID in the database representing the board owner.
     */
    private String ownerUid;

    public Board(String ownerUid, String name, String description) {
        super(name, description);
        this.ownerUid = ownerUid;
    }

    public Board(String ownerUid, String name, String description, Timestamp createdAt, Timestamp lastEditAt) {
        super(name, description, createdAt, lastEditAt);
        this.ownerUid = ownerUid;
    }

    /**
     * Creates the {@link Board} object from the data (attribute values) that are on the map.
     *
     * @param data
     *         attributes values.
     *
     * @return cz.uhk.umte.kruncja1.notes.database.entity.Board instance containing values from the
     * data map.
     */
    public static Board getBoardFromMap(Map<String, Object> data) {
        final String ownerUid = (String) data.get("ownerUid");
        final String name = (String) data.get("name");
        final String description = (String) data.get("description");
        final Timestamp createdAt = (Timestamp) data.get("createdAt");
        final Timestamp lastEditAt = (Timestamp) data.get("lastEditAt");

        return new Board(ownerUid, name, description, createdAt, lastEditAt);
    }

    // Below are the items needed to pass an instance of this class to Intent because TimeStamp is not serializable. Therefore, implementation of values ​​within the Parcelable:

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // This is needed (done) when passing an instance of this class to Intent, so you need to "write" all values ​​(serialization replacement because TimeStamp is not serializable):
        dest.writeString(ownerUid);
        dest.writeString(name);
        dest.writeString(description);

        dest.writeInt(createdAt.getNanoseconds());
        dest.writeLong(createdAt.getSeconds());

        dest.writeInt(lastEditAt.getNanoseconds());
        dest.writeLong(lastEditAt.getSeconds());
    }

    public static final Parcelable.Creator<Board> CREATOR = new Parcelable.Creator<Board>() {
        public Board createFromParcel(Parcel in) {
            return new Board(in);
        }

        public Board[] newArray(int size) {
            return new Board[size];
        }
    };

    /**
     * The constructor is called when data is obtained from Intent and specific instances of this
     * class is created with the relevant data.
     *
     * @param in
     */
    private Board(Parcel in) {
        ownerUid = in.readString();
        name = in.readString();
        description = in.readString();

        final int iCreatedAt = in.readInt();
        final long lCreatedAt = in.readLong();
        createdAt = new Timestamp(lCreatedAt, iCreatedAt);

        final int iLastEditAt = in.readInt();
        final long lLastEditAt = in.readLong();
        lastEditAt = new Timestamp(lLastEditAt, iLastEditAt);
    }
}