package cz.uhk.umte.kruncja1.notes.texthelper;

import com.google.firebase.Timestamp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Only for ancillary work with the text, there is no implementation of the interface, the
 * individual methods should not need anything else, not to go anywhere, etc.
 */

public class TextHelper {

    public static final String REG_EX_REPLACEMENT_TELEPHONE_NUMBER_WITH_SPACE = "$1 $2 $3";
    public static final String REG_EX_REPLACEMENT_TELEPHONE_NUMBER_WITH_CODE = "(+420) $1 $2 $3";

    public static String getTextFromTimeStamp(Timestamp timestamp) {
        final TimeZone timeZone = TimeZone.getTimeZone("Europe/Prague");

        final Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp.getSeconds() * 1000L);
        c.setTimeZone(timeZone);

        final Date d = c.getTime();
        final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. yyyy HH:mm:ss", Locale.US);
        sdf.setTimeZone(timeZone);
        return sdf.format(d);
    }

    /**
     * Format the phone number to the syntax given by the regular expression.
     *
     * @param telephoneNumber
     *         phone number to be formatted.
     *
     * @return phone number (telephoneNumber) in the form specified by the regular expression.
     */
    public static String formatTelephoneNumber(String telephoneNumber, String regEx) {
        return telephoneNumber.replaceFirst("(\\d{3})(\\d{3})(\\d{3})", regEx);
    }
}