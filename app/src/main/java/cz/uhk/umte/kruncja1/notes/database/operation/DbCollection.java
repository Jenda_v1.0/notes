package cz.uhk.umte.kruncja1.notes.database.operation;

/**
 * Database collection names in FirebaseFirestore.
 */

public enum DbCollection {

    USER("users"),
    BOARD("boards"),
    TOPIC("topics"),
    NOTE("notes"),
    TEAM("teams");

    /**
     * The name of the collection in the database.
     */
    private final String name;

    DbCollection(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}